import 'dart:collection';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/string_handling.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';


class SignInAlert extends StatefulWidget {
  final double screenHeight;
  final bool isNewUser;
  final GlobalKey<ScaffoldState> scaffoldKey;

  SignInAlert({
    @required this.screenHeight,
    @required this.isNewUser,
    @required this.scaffoldKey
  });

  @override
  _SignInAlertState createState() => _SignInAlertState(screenHeight, isNewUser, scaffoldKey);
}

class _SignInAlertState extends State<SignInAlert> {

  _SignInAlertState(this.screenHeight, this.isNewUser, this.scaffoldKey);
  final double screenHeight;
  final bool isNewUser;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final givenNameController = TextEditingController();
  final familyNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey;
  bool errorWithLogin = false;
  bool isPasswordReset = false;
  String errorText = "Looks like you've entered incorrect details. Please try again.";

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    familyNameController.dispose();
    givenNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    String signInText = isNewUser ? "Register" : "Sign In";
    if(isPasswordReset) signInText = "Please enter your account email";

    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kAlertBorderRadius),
      ),
      title: Padding(
        padding: EdgeInsets.only(top: height * 0.2),
        child: Text(
          signInText,
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: kTitleTextSize
          ),
        ),
      ),
      content: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(minWidth: width * 0.9, maxWidth:  width * 0.9 ),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Visibility(
                  visible: errorWithLogin,
                  child: Text(
                    errorText,
                    style: kAlertErrorTextStyle,
                  ),
                ),
                isNewUser ? Padding(
                  padding: EdgeInsets.symmetric(vertical: screenHeight * 0.03),
                  child: TextFormField(
                    autofocus: isNewUser ? true : false,
                    cursorColor: Theme.of(context).primaryColor,
                    controller: givenNameController,
                    decoration: InputDecoration(
                      hintText: "First Name",
                      labelStyle: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    validator: (value) {
                      value = value.trim();
                      if (value.isEmpty) {
                        return "Please enter some text";
                      }
                      return null;
                    },
                  ),
                ) : SizedBox(),
                isNewUser ? Padding(
                  padding: EdgeInsets.symmetric(vertical: screenHeight * 0.03),
                  child: TextFormField(
                    autofocus: false,
                    cursorColor: Theme.of(context).primaryColor,
                    controller: familyNameController,
                    decoration: InputDecoration(
                      hintText: "Last Name",
                      labelStyle: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    validator: (value) {
                      value = value.trim();
                      if (value.isEmpty) {
                        return "Please enter some text";
                      }
                      return null;
                    },
                  ),
                ) : SizedBox(),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: screenHeight * 0.03),
                  child: TextFormField(
                    autofocus: isNewUser ? false : true,
                    cursorColor: Theme.of(context).primaryColor,
                    controller: emailController,
                    decoration: InputDecoration(
                      hintText: "Email Address",
                      labelStyle: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    validator: (value) {
                      value = value.trim();
                      if (value.isEmpty || EmailValidator.validate(value) == false) {
                        return "Please enter a valid email address";
                      }
                      return null;
                    },
                  ),
                ),
                !isPasswordReset ? Padding(
                  padding: EdgeInsets.only(top: screenHeight * 0.03),
                  child: TextFormField(
                    obscureText: true,
                    cursorColor: Theme.of(context).primaryColor,
                    controller: passwordController,
                    decoration: InputDecoration(
                      hintText: "Password",
                      labelStyle: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Please enter some text";
                      }
                      return null;
                    },
                  ),
                ) : SizedBox(),
              ],
            ),
          ),
        ),
      ),
      actions: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            FlatButton(
              child: Text(
                !isPasswordReset ? signInText : "Reset Password",
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: kSmallTextSize
                ),
              ),
              onPressed: (){
                if(_formKey.currentState.validate()){
                  try{
                    if(!isPasswordReset){
                      isNewUser ? register() : signIn();
                    } else {
                      resetPassword(emailController.text);
                    }
                  } catch (e){
                    print("ERROR");
                    print(e);
                  }
                }
              },
            ),
            !isNewUser ? FlatButton(
              child: Text(
                !isPasswordReset ? "Forgot Password?" : "Back to Sign In",
                style: TextStyle(
                    color: Theme.of(context).accentColor.withOpacity(0.5),
                    fontSize: kSmallerTextSize
                ),
              ),
              onPressed: (){
                  try{
                    setState(() {
                      isPasswordReset = !isPasswordReset;
                    });
                  } catch (e){
                    print("ERROR");
                    print(e);
                  }
              },
            ) : SizedBox(),
          ],
        ),
      ],
    );
  }

  void register() async {
    try{
      //First register the user
      final FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
        email: emailController.text.trim(),
        password: passwordController.text.trim(),
      )).user;

      //Then sign the user in
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailController.text.trim(),
          password: passwordController.text.trim(),
      );
      //Create database entries for the user
      await createUserEntry(user, capitalize(givenNameController.text.trim()), capitalize(familyNameController.text.trim()));
      Map<String,dynamic> data = await getUserData();
      await assignUserData(data, context, true);
      //redirect the user to task screen
      Navigator.pushReplacementNamed(context, 'tasks');
    } catch (e) {
      print(e);
      if(passwordController.value.text.length < 6){
        setState(() {
          errorText = "Passwords must have at least 6 characters.";
          errorWithLogin = true;
        });
      }
    }

  }

  void signIn() async {
    try{
     FirebaseUser user = (await _auth.signInWithEmailAndPassword(
          email: emailController.text.trim(),
          password: passwordController.text.trim()
      )).user;
      HashMap<String,dynamic> userData = await getUserData();
      await assignUserData(userData, context, true);
      if(user != null){
        Navigator.pushReplacementNamed(context, 'balance');
      }
    } catch (e){
      print(e);
      setState(() {
        print(errorWithLogin);
        emailController.clear();
        passwordController.clear();
        errorWithLogin = true;
      });
    }
  }

  resetPassword(String email) async {
    await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
    scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            "Reset link sent.",
            style: TextStyle(
              color: Colors.white
            ),
          ),
          backgroundColor: Theme.of(context).primaryColor,
        ),
    );
    Navigator.pop(context);
  }

}

