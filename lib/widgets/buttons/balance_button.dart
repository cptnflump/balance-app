import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class BalanceButton extends StatefulWidget {
  const BalanceButton({
    @required this.width,
    @required this.height,
    @required this.onTap,
    @required this.text,
    @required this.icon,
    this.iconSizeMultiplier=1.0,
    this.paddingSizeMultiplier=1.0,
  });

  final double width;
  final double height;
  final Function onTap;
  final String text;
  final IconData icon;
  final double iconSizeMultiplier;
  final double paddingSizeMultiplier;

  @override
  _BalanceButtonState createState() => _BalanceButtonState();
}

class _BalanceButtonState extends State<BalanceButton> {

  @override
  Widget build(BuildContext context) {

    double smallestDimension = min(widget.height, widget.width);

    return Column(
          children: [
            RawMaterialButton(
              splashColor: Theme.of(context).primaryColor,
              onPressed: widget.onTap,
              elevation: 5.0,
              fillColor: Colors.white,
              child: Icon(
                widget.icon,
                color: Theme.of(context).primaryColor,
                size: (smallestDimension * 0.08) * widget.iconSizeMultiplier,
              ),
              padding: EdgeInsets.all((smallestDimension * 0.03) * widget.paddingSizeMultiplier),
              shape: CircleBorder(),
            ),
            SizedBox(
              height: widget.height * 0.02,
            ),
            AutoSizeText(
              widget.text,
              maxLines: 1,
              style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: kMainTextSize,
                  color: Theme.of(context).accentColor
              ),
            ),
          ],
        );
  }
}