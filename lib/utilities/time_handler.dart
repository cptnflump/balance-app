DateTime startOfDay(DateTime date){
  return DateTime(date.year, date.month, date.day, 0, 0, 0, 0, 0);
}

/// The last day of a given month
DateTime lastDayOfMonth(int month) {
var beginningNextMonth = (month < 12)
? new DateTime(DateTime.now().year, month + 1, 1)
    : new DateTime(DateTime.now().year + 1, 1, 1);
return beginningNextMonth.subtract(new Duration(days: 1));
}

String createTimedImageAsset(){
  int hour = DateTime.now().hour;
  if(hour >= 0 && hour < 12){
    return "images/greeting_morning.png";
  } else if(hour < 18){
    return "images/greeting_afternoon.png";
  } else return "images/greeting_evening.png";
}

String getDaySuffix(int day){
  if(day.toString().endsWith("1")){
    return "st";
  } else if(day.toString().endsWith("2")){
    return "nd";
  } else if(day.toString().endsWith("3")){
    return "rd";
  } else {
    return "th";
  }
}

String getMonth(int month){
  switch(month){
    case 1:
      return "January";
      break;
    case 2:
      return "February";
      break;
    case 3:
      return "March";
      break;
    case 4:
      return "April";
      break;
    case 5:
      return "May";
      break;
    case 6:
      return "June";
      break;
    case 7:
      return "July";
      break;
    case 8:
      return "August";
      break;
    case 9:
      return "September";
      break;
    case 10:
      return "October";
      break;
    case 11:
      return "November";
      break;
    case 12:
      return "December";
      break;
    default:
      return "";
  }
}