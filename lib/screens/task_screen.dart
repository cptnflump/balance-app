import 'dart:collection';
import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/project.dart';
import 'package:balance_app/utilities/task.dart';
import 'package:balance_app/utilities/time_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/alerts/task_alerts/task_modal.dart';
import 'package:balance_app/widgets/alerts/time_away_alert.dart';
import 'package:balance_app/widgets/blur_screen.dart';
import 'package:balance_app/widgets/navigation_bar.dart';
import 'package:balance_app/widgets/task_screen_drawer.dart';
import 'package:balance_app/widgets/lists/task_list_widget.dart';
import 'package:balance_app/widgets/tutorials/task_screen/icon_tutorial.dart';
import 'package:balance_app/widgets/tutorials/task_screen/task_tutorial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class TaskScreen extends StatefulWidget {
  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen>  with WidgetsBindingObserver{
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double smallestDimension = min(screenHeight,screenWidth);

    userTasks = TaskListHandler().orderTasks();


    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        floatingActionButton: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Visibility(
              visible: listFilter != null,
              child: FloatingActionButton(
                heroTag: "clearFilter",
                elevation: 5,
                backgroundColor: Theme.of(context).primaryColor,
                child: Icon(Icons.clear_all, color: Colors.white, size: (smallestDimension * 0.08),),
                onPressed: (){
                  listFilter = null;
                  isFilteredByProject = false;
                  filteredUserTasks = userTasks;
                  Navigator.pushReplacementNamed(context,'tasks');
                },
              ),
            ),
            SizedBox(height: screenHeight * 0.05,),
            FloatingActionButton(
              heroTag: "addTask",
              elevation: 5,
              backgroundColor: Theme.of(context).primaryColor,
              child: Icon(Icons.add, color: Colors.white, size: (smallestDimension * 0.08),),
              onPressed: (){
                currProject = null;
                processNewTask(screenHeight);
              },

            ),
          ],
        ),
        key: _scaffoldKey,
        drawer: TaskScreenDrawer(screenHeight: screenHeight, screenWidth: screenWidth),
        appBar: AppBar(
          title: Text(
            "Tasks",
            style: TextStyle(
              fontSize: kTitleTextSize,
              color: Colors.white,
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          leading: IconButton(
            icon: Icon(Icons.menu, size: 30,),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          ),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        resizeToAvoidBottomPadding: false,
        bottomNavigationBar: NavigationBar(
          disableTask: true,
        ),
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Expanded(
                      child: RefreshIndicator(
                        color: Theme.of(context).primaryColor,
                        onRefresh: (){
                          updateCompleted();
                          return refreshTasks();
                          },
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            if (userTasks.isEmpty) {
                              return Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: screenWidth * 0.1),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      AutoSizeText(
                                        "Nothing here yet...",
                                        maxLines: 1,
                                        style: TextStyle(fontSize: kHugeTextSize, color: Theme.of(context).accentColor),
                                      ),
                                      SizedBox(
                                        height: screenHeight * 0.01,
                                      ),
                                      AutoSizeText(
                                        "Tap the plus to get started.",
                                        maxLines: 1,
                                        style: TextStyle(fontSize: kTitleTextSize, color: Theme.of(context).accentColor),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            } else {
                              return TaskListWidget();
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: !userHasSeenTask,
                child: BlurScreen(
                  child: TaskTutorial(
                    onTap: (){
                      userHasSeenTask = true;
                      finishTaskTutorial();
                      setState(() {});
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void processNewTask(double screenHeight) async {

    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: const Radius.circular(10), topRight: const Radius.circular(10)),
        ),
        context: context,
        builder: (context) => TaskModal());
    if(!userHasCreatedTask){
      await showDialog(
        context: context,
        builder: (context) => IconTutorial(),
      );
    }
  }



  Future<void> refreshTasks() async {
    HashMap<String,dynamic> userData = await getUserData();
    await assignUserData(userData, null, false);
    if(listFilter != null) listFilter = null;
    if(isFilteredByProject != null && isFilteredByProject) isFilteredByProject = false;
    setState(() {});
  }

  bool userHasOpenedAppToday(){
    print(startOfDay(DateTime.now()).difference(startOfDay(userLastAppOpen)).inDays);
    return startOfDay(DateTime.now()).difference(startOfDay(userLastAppOpen)).inDays == 0;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('MyApp state = $state');
    if (state == AppLifecycleState.inactive) {
      // app transitioning to other state.
    } else if (state == AppLifecycleState.paused) {
      // app is on the background.
      setState(() {
        updateCompleted();
      });
    } else if (state == AppLifecycleState.detached) {
      // flutter engine is running but detached from views
    } else if (state == AppLifecycleState.resumed) {
      // app is visible and running.
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        print(DateTime.now().difference(userLastAppOpen).inHours);
        if(userLastAppOpen != null && userHasSeenBalance && DateTime.now().difference(userLastAppOpen).inHours > 1 && userHasOpenedAppToday()){
          await showDialog(
            context: context,
            builder: (context) => TimeAwayAlert(scaffoldKey: _scaffoldKey,),
          );
          setState(() {});
          logAppOpen();
          userLastAppOpen = DateTime.now();
        } else {
          logAppOpen();
          userLastAppOpen = DateTime.now();
        }
      });
    }
  }

}

class TaskListHandler{
  List orderTasks(){


    //first create a list of all the due dates in the current task list
    List dates = [];
    userTasks.forEach((element) {
      if(!dates.contains(element.taskDue) && element.taskDue != null){
        dates.add(element.taskDue);
      }
    });
    dates.sort();

    List<Task> tasks = [];
    //remove completed tasks
    userTasks.removeWhere((element) => element.isComplete);

    //now loop through the date list, for each element add tasks that are priority, then the ones that arent
    dates.forEach((date) {
      tasks.addAll(userTasks.where((element) => element.taskDue == date && element.isPriority));
      tasks.addAll(userTasks.where((element) => element.taskDue == date && !element.isPriority));
    });

    //now add all elements that have no due date and are priority
    tasks.addAll(userTasks.where((element) => element.taskDue == null && element.isPriority));
    //now add all elements that have no due date and are not priority
    tasks.addAll(userTasks.where((element) => element.taskDue == null && !element.isPriority));

    return tasks;
  }
}


