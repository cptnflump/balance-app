import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'package:apple_sign_in/apple_sign_in.dart' as AppleSignIn;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/screens/manual_sign_in_screen.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/buttons/sign_in_button.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';

class SignIn extends StatelessWidget {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;

    Future<bool> canAppleSignIn = checkCanAppleSignIn(context);

    return Scaffold(
      key: scaffoldKey,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
            children: [
              SizedBox(height: screenHeight * 0.1,),
              Container(
                height: screenHeight * 0.2,
                child: AspectRatio(
                  aspectRatio: 3/2,
                  child: Image.asset(Theme.of(context).brightness == Brightness.light ? 'images/blossum.png' : 'images/blossum_dark.png'),
                ),
              ),
            ],
          ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
////                  APPLE BUTTON
//                Container(
//                  child: AppleSignIn.AppleSignInButton(
//                    type: AppleSignIn.ButtonType.signIn,
//                    style: AppleSignIn.ButtonStyle.whiteOutline,
//                  ),
//                  width: screenWidth * 0.6,
//                ),
                  FutureBuilder(
                    future: canAppleSignIn,
                    builder: (context, snapshot){
                      if(snapshot.connectionState == ConnectionState.done){
                        if(snapshot.hasError){
                          return SizedBox();
                        } else {
                          if(snapshot.data == true){
                            return GestureDetector(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Theme.of(context).brightness == Brightness.dark ? Colors.black : Colors.white,
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: screenHeight * 0.02, horizontal:  screenWidth * 0.05),
                                  child: SizedBox(
                                    height: screenHeight * 0.04,
                                    width: screenWidth * 0.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Flexible(
                                        child: FaIcon(
                                          FontAwesomeIcons.apple,
                                          color: Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white,
                                        ),
                                      ),
                                      SizedBox(
                                        width: screenWidth * 0.02,
                                      ),
                                      Container(
                                        width: screenWidth * 0.35,
                                        child: AutoSizeText(
                                          "Sign in with Apple",
                                          maxLines: 1,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: kSmallerTextSize, color: Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                ),
                              ),
                              onTap: (){
                                appleSignIn(context);
                              },
                            );
                          } else {
                            return SizedBox();
                          }
                        }
                      } else {
                        return SizedBox();
                      }
                    },
                  ),
                  SizedBox(
                    height: screenHeight * 0.05,
                  ),
              GestureDetector(
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.black : Colors.white,
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: screenHeight * 0.02, horizontal:  screenWidth * 0.05),
                    child: SizedBox(
                      width: screenWidth * 0.5,
                      height: screenHeight * 0.04,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            child: Image.asset("images/google_logo.png")
                          ),
                          SizedBox(
                            width: screenWidth * 0.02,
                          ),
                          Container(
                            width: screenWidth * 0.35,
                            child: AutoSizeText(
                              "Sign in with Google",
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: kSmallerTextSize, color: Theme.of(context).brightness == Brightness.light ? Colors.black : Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                onTap: (){
                  googleSignIn(context);
                },
              ),
                  SizedBox(
                    height: screenHeight * 0.05,
                  ),
                  SignInButton(
                    child: SizedBox(
                      width: screenWidth * 0.5,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            child: FaIcon(
                              FontAwesomeIcons.solidEnvelope,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                          SizedBox(
                            width: screenWidth * 0.02,
                            height: screenHeight * 0.04,
                          ),
                          Container(
                            width: screenWidth * 0.35,
                            child: AutoSizeText(
                              "Sign in with Email",
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              style: TextStyle(fontSize: kSmallerTextSize),
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => ManualSignInScreen(isNewUser: false, scaffoldKey: scaffoldKey,),
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: screenHeight * 0.05,
                  ),
                  FlatButton(
                    child: Text(
                      "Register",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor, fontSize: kSmallTextSize),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => ManualSignInScreen(isNewUser: true, scaffoldKey: scaffoldKey,),
                        ),
                      );
                    },
                  ),
                  SizedBox(height: screenHeight * 0.05,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void appleSignIn(BuildContext context) async {
// this bool will be true if apple sign in is enabled
        final AppleSignIn.AuthorizationResult result = await AppleSignIn.AppleSignIn.performRequests([
          AppleSignIn.AppleIdRequest(requestedScopes: [AppleSignIn.Scope.email, AppleSignIn.Scope.fullName])
        ]);
        switch (result.status) {
          case AppleSignIn.AuthorizationStatus.authorized:
            print("successfull sign in");
            final AppleSignIn.AppleIdCredential appleIdCredential = result.credential;

            String givenName = appleIdCredential.fullName.givenName;
            String familyName = appleIdCredential.fullName.familyName;
            userGivenName = givenName;
            userFamilyName = familyName;

            OAuthProvider oAuthProvider =
            new OAuthProvider(providerId: "apple.com");
            final AuthCredential credential = oAuthProvider.getCredential(
              idToken:
              String.fromCharCodes(appleIdCredential.identityToken),
              accessToken:
              String.fromCharCodes(appleIdCredential.authorizationCode),
            );

            final   _res = await FirebaseAuth.instance
                .signInWithCredential(credential);

           FirebaseUser user = await FirebaseAuth.instance.currentUser();

            DocumentSnapshot ds = await Firestore.instance
                .collection(fbUsersTable)
                .document(user.uid)
                .get();
            if(null == ds.data){
              await createUserEntry(user, givenName, familyName);
            }
            HashMap<String,dynamic> userData = await getUserData();
            await assignUserData(userData, context, true).then((value) => Navigator.pushReplacementNamed(context, 'balance'));

            break;
          case AppleSignIn.AuthorizationStatus.error:
            print("Error with Apple Sign in");
            print(result.error);
            break;

          case AppleSignIn.AuthorizationStatus.cancelled:
            print('User cancelled');
            break;
        }
  }

  Future<bool> checkCanAppleSignIn(BuildContext context) async{
    bool supportsAppleSignIn = false;
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      supportsAppleSignIn = await AppleSignIn.AppleSignIn.isAvailable();
    }
    return supportsAppleSignIn;
  }

  void googleSignIn(BuildContext context) async {
    try{
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth = await googleUser.authentication;


//    This can give us the users given and family name
      String authPayload = googleAuth.idToken.split(".")[1];
      String normalised = base64Url.normalize(authPayload);
      Map<String,dynamic> tokenDetails = json.decode(utf8.decode(base64Decode(normalised)));
      String givenName = tokenDetails["given_name"];
      String familyName = tokenDetails["family_name"];

      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      final FirebaseUser user =
          (await _auth.signInWithCredential(credential)).user;
      DocumentSnapshot ds = await Firestore.instance
          .collection(fbUsersTable)
          .document(user.uid)
          .get();
      if(null == ds.data){
        await createUserEntry(user, givenName, familyName);
      }
      HashMap<String,dynamic> userData = await getUserData();
      await assignUserData(userData, context, true);
      Navigator.pushReplacementNamed(context, 'balance');
    } catch (e){
      print(e);
    }

  }
}
