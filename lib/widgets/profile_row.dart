import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/widgets/image_widgets/sized_icon.dart';
import 'package:flutter/material.dart';

class ProfileRow extends StatelessWidget {
  const ProfileRow({
    @required this.screenHeight,
    @required this.screenWidth,
    @required this.icon,
    @required this.rowText,
    @required this.onTap,
  });

  final double screenHeight;
  final double screenWidth;
  final IconData icon;
  final String rowText;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.transparent,
        height: screenHeight * 0.07,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.05),
              child: SizedIcon(
                icon: icon,
                colour: Theme.of(context).accentColor,
                parentChildRatio: 0.3,
              ),
            ),
            AutoSizeText(
              rowText,
              maxLines: 1,
              style: TextStyle(
                fontSize: kSmallTextSize,
                color: Theme.of(context).accentColor
              ),
            )
          ],
        ),
      ),
    );
  }
}