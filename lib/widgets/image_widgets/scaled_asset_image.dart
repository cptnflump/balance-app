import 'package:flutter/material.dart';

class ScaledAssetImage extends StatelessWidget {

  final String imageName;

  ScaledAssetImage({@required this.imageName});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints){
        return Image.asset(
          imageName,
          height: constraints.maxHeight,
        );
      },
    );
  }
}
