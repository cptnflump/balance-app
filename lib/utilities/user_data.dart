import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/task.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

String userUid;
String userFamilyName;
String userGivenName;
DateTime userJoinedOn;
List<Task> userTasks = [];
List<Task> filteredUserTasks = [];
List<Task> userCompletedTasks = [];
List<String> userProjects = [];
bool userIsSubscriber = false;
String userEmail;
double userBalance;
double userGoal;
int userNumberCompletedTasks;
int userNumberCompletedBalance;
int userLifetimeCompletedTasks;
int userLifetimeCompletedBalance;
DateTime userLastCheckIn;
DateTime userLastAppOpen;
// below is used for filtering listview
String listFilter;
bool isFilteredByProject;
bool userHasCreatedTask;
bool userHasSeenTask;
bool userHasSeenBalance;

Future<void> assignUserData(Map<String,dynamic> data, BuildContext context, bool loadPurchases) async{
  try{
    userProjects = createProjects(data[fbUserProjects]);
    userUid = data[fbUid];
    userTasks = createTasks(data[fbTasksField]);
    filteredUserTasks = userTasks;
    userEmail = data[fbUserEmail];
    userNumberCompletedTasks = data[fbUserCompletedTasks];
    userBalance = data[fbUserBalance].toDouble();
    userGoal = data[fbUserGoal].toDouble();
    userNumberCompletedBalance = data[fbUserCompletedBalance];
    userLifetimeCompletedBalance = data[fbLifeTimeBalanceTasks];
    userLifetimeCompletedTasks = data[fbLifeTimeBalanceTasks];
    userLastCheckIn = data[fbUserLastCheckIn]  == null ? null :(data[fbUserLastCheckIn] as Timestamp).toDate();
    userFamilyName = data[fbUserFamilyName];
    userGivenName = data[fbUserGivenName];
    userJoinedOn = (data[fbUserJoinedOn] as Timestamp).toDate();
    userHasCreatedTask = (data[fbUserHasCreatedTask]);
    userHasSeenTask = (data[fbUserHasSeenTask]);
    userHasSeenBalance = (data[fbUserHasSeenBalance]);
    userLastAppOpen = data[fbLastAppOpen] == null ? null : (data[fbLastAppOpen] as Timestamp).toDate();
    if(loadPurchases) await initialisePurchases(userUid);
    userIsSubscriber = await isSubscriber();
  } catch (e){
    print("Error creating user data. Signing out.");
    print(e);
    wipeSessionData();
    FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, 'signIn');
  }

}

initialisePurchases(String uid) async{
  Purchases.setDebugLogsEnabled(true);
  Purchases.setup(kRevenueCatPublicKey, appUserId: uid);
}

void updateCompleted(){
  if(userCompletedTasks.isNotEmpty){
    print(userCompletedTasks.length);
    userCompletedTasks.forEach((element) {
      if(filteredUserTasks.contains(element)) filteredUserTasks.remove(element);
      userTasks.remove(element);
      incrementTaskNumber(isBalance: element.isBalance);
    });
    userCompletedTasks.clear();
    updateTasks();
  }
}

void wipeSessionData(){
  userUid = null;
  userTasks.clear();
  userCompletedTasks.clear();
  filteredUserTasks.clear();
  userIsSubscriber = false;
  userEmail = null;
  userNumberCompletedTasks = null;
  userBalance = null;
  userGoal = null;
  userNumberCompletedBalance = null;
  userProjects = null;
  userLastCheckIn = null;
  userLifetimeCompletedTasks = null;
  userLifetimeCompletedBalance = null;
  userJoinedOn = null;
  userHasSeenBalance = null;
  userHasCreatedTask = null;
  userHasSeenTask = null;
}

List<Task> createTasks(Map<String, dynamic> taskData){
  List<Task> tasks = [];
  taskData.forEach((key, value) {
    if(value[fbIsComplete] == false){
      Timestamp dueDate = value[fbTaskDue];
      tasks.add(Task(
          taskTitle: key.split("<SEP>")[0],
          isPriority: value[fbIsPriority],
          isBalance: value[fbIsBalance],
          taskDue: dueDate == null ? null : dueDate.toDate(),
          isComplete: value[fbIsComplete],
          project: value[fbTaskProject],
      ));
    }
  });
  return tasks;
}

List<String> createProjects(List<dynamic> data){
  return data.cast<String>().toList();
}

void signOut(BuildContext context) async {
  Purchases.reset();
  await FirebaseAuth.instance.signOut();
  wipeSessionData();
  Navigator.pushReplacementNamed(context, 'signIn');
}

void deleteAccount(BuildContext context) async {

  try{
   FirebaseUser user = await FirebaseAuth.instance.currentUser();
    print(user.uid);
    wipeSessionData();
    await deleteUserEntry();
    await user.delete();
    Navigator.pushReplacementNamed(context, 'signIn');
  } catch(e){
    print(e);
  }
}

Future<bool> isSubscriber() async{
  bool subscriberOverride = await forcedSubscriber();
  if(subscriberOverride){
    return true;
  } else{
    PurchaserInfo info = await Purchases.getPurchaserInfo();
    print(info.entitlements.active);
    return(info.entitlements.active.containsKey("Pro"));
  }
}

