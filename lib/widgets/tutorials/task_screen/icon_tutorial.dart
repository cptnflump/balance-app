import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/widgets/alerts/task_alerts/task_modal.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class IconTutorial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
        side: BorderSide(color: Theme.of(context).primaryColor, width: 1.5),
      ),
      title: Text(
        "Task Icons",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: kMainTextSize,
          color: Theme.of(context).primaryColor,
        ),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              Icon(
                FontAwesomeIcons.calendarAlt,
                color: Theme.of(context).accentColor,
              ),
              SizedBox(width: width * 0.1,),
              Flexible(
                child: Text(
                  "Tap the calendar icon to add or remove a due date to your task.",
                  style: TextStyle(
                    fontSize: kSmallerTextSize,
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: height * 0.025,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              Icon(
                FontAwesomeIcons.fireAlt,
                color: Theme.of(context).accentColor,
              ),
              SizedBox(width: width * 0.1,),
              Flexible(
                child: Text(
                  "Tap the fire icon to make the task a priority, ranking it higher.",
                  style: TextStyle(
                    fontSize: kSmallerTextSize,
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: height * 0.025,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              Icon(
                FontAwesomeIcons.spa,
                color: Theme.of(context).accentColor,
              ),
              SizedBox(width: width * 0.1,),
              Flexible(
                child: Text(
                  "Tap the lotus icon to make this a rest task. This will count towards rest in your balance.",
                  style: TextStyle(
                    fontSize: kSmallerTextSize,
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: height * 0.025,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              Icon(
                FontAwesomeIcons.tag,
                color: Theme.of(context).accentColor,
              ),
              SizedBox(width: width * 0.1,),
              Flexible(
                child: Text(
                  "Tap the tag icon to assign this task to a project.",
                  style: TextStyle(
                    fontSize: kSmallerTextSize,
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: height * 0.03,),
          GestureDetector(
            onTap: (){
              Navigator.pop(context);
              Timer(const Duration(milliseconds: 1), () {
                taskTitleFocusNode.requestFocus();
              });
            },
            child: Text(
              "Okay",
              style: TextStyle(
                  fontSize: kMainTextSize,
                  color: Theme.of(context).primaryColor
              ),
            ),
          ),
        ],
      ),
    );
  }
}
