import 'dart:collection';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/string_handling.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';


class ManualSignInScreen extends StatefulWidget {
  final bool isNewUser;
  final GlobalKey<ScaffoldState> scaffoldKey;

  ManualSignInScreen({
    @required this.isNewUser,
    @required this.scaffoldKey
  });

  @override
  _ManualSignInScreenState createState() => _ManualSignInScreenState(isNewUser, scaffoldKey);
}

class _ManualSignInScreenState extends State<ManualSignInScreen> {

  _ManualSignInScreenState(this.isNewUser, this.scaffoldKey);
  final bool isNewUser;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final givenNameController = TextEditingController();
  final familyNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey;
  bool errorWithLogin = false;
  bool isPasswordReset = false;
  String errorText = "Looks like you've entered incorrect details. Please try again.";

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    familyNameController.dispose();
    givenNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    
    String signInText = isNewUser ? "Register" : "Sign In";
    if(isPasswordReset) signInText = "Please enter your account email";

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(height: height * 0.1,),
                  Text(
                    signInText,
                    style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontSize: kTitleTextSize
                    ),
                  ),
                  Visibility(
                    visible: errorWithLogin,
                    child: Column(
                      children: [
                        SizedBox(height: height * 0.02,),
                        Text(
                          errorText,
                          style: kAlertErrorTextStyle,
                        ),
                      ],
                    ),
                  ),
                  isNewUser ? Padding(
                    padding: EdgeInsets.symmetric(vertical: height * 0.03),
                    child: TextFormField(
                      autofocus: isNewUser ? true : false,
                      cursorColor: Theme.of(context).primaryColor,
                      controller: givenNameController,
                      decoration: InputDecoration(
                        hintText: "First Name",
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: kOrange),
                        ),
                      ),
                      validator: (value) {
                        value = value.trim();
                        if (value.isEmpty) {
                          return "Please enter some text";
                        }
                        return null;
                      },
                    ),
                  ) : SizedBox(),
                  isNewUser ? Padding(
                    padding: EdgeInsets.symmetric(vertical: height * 0.03),
                    child: TextFormField(
                      autofocus: false,
                      cursorColor: Theme.of(context).primaryColor,
                      controller: familyNameController,
                      decoration: InputDecoration(
                        hintText: "Last Name",
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: kOrange),
                        ),
                      ),
                      validator: (value) {
                        value = value.trim();
                        if (value.isEmpty) {
                          return "Please enter some text";
                        }
                        return null;
                      },
                    ),
                  ) : SizedBox(),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: height * 0.03),
                    child: TextFormField(
                      autofocus: isNewUser ? false : true,
                      cursorColor: Theme.of(context).primaryColor,
                      controller: emailController,
                      decoration: InputDecoration(
                        hintText: "Email Address",
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: kOrange),
                        ),
                      ),
                      validator: (value) {
                        value = value.trim();
                        if (value.isEmpty || EmailValidator.validate(value) == false) {
                          return "Please enter a valid email address";
                        }
                        return null;
                      },
                    ),
                  ),
                  !isPasswordReset ? Padding(
                    padding: EdgeInsets.only(top: height * 0.03),
                    child: TextFormField(
                      obscureText: true,
                      cursorColor: Theme.of(context).primaryColor,
                      controller: passwordController,
                      decoration: InputDecoration(
                        hintText: "Password",
                        labelStyle: TextStyle(color: Theme.of(context).primaryColor),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: kOrange),
                        ),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter some text";
                        }
                        return null;
                      },
                    ),
                  ) : SizedBox(),
                  SizedBox(height: height * 0.05,),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      FlatButton(
                        child: Text(
                          !isPasswordReset ? signInText : "Reset Password",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: kSmallTextSize
                          ),
                        ),
                        onPressed: (){
                          FocusScope.of(context).unfocus();
                          if(_formKey.currentState.validate()){
                            try{
                              if(!isPasswordReset){
                                isNewUser ? register() : signIn();
                              } else {
                                resetPassword(emailController.text);
                              }
                            } catch (e){
                              print("ERROR");
                              print(e);
                            }
                          }
                        },
                      ),
                      !isNewUser ? FlatButton(
                        child: Text(
                          !isPasswordReset ? "Forgot Password?" : "Back to Sign In",
                          style: TextStyle(
                              color: Theme.of(context).accentColor.withOpacity(0.5),
                              fontSize: kSmallerTextSize
                          ),
                        ),
                        onPressed: (){
                          try{
                            setState(() {
                              isPasswordReset = !isPasswordReset;
                            });
                          } catch (e){
                            print("ERROR");
                            print(e);
                          }
                        },
                      ) : SizedBox(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void register() async {
    try{
      //First register the user
      final FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
        email: emailController.text.trim(),
        password: passwordController.text.trim(),
      )).user;

      //Then sign the user in
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailController.text.trim(),
        password: passwordController.text.trim(),
      );
      //Create database entries for the user
      await createUserEntry(user, capitalize(givenNameController.text.trim()), capitalize(familyNameController.text.trim()));
      Map<String,dynamic> data = await getUserData();
      await assignUserData(data, context, true);
      //redirect the user to task screen
      Navigator.pushReplacementNamed(context, 'balance');
    } catch (e) {
      if(e.toString().contains("ERROR_EMAIL_ALREADY_IN_USE")){
        setState(() {
          errorText = "That Email has already been taken.";
          errorWithLogin = true;
        });
      } else if(passwordController.value.text.length < 6){
        setState(() {
          errorText = "Passwords must have at least 6 characters.";
          errorWithLogin = true;
        });
      }
    }

  }

  void signIn() async {
    try{
      FocusScope.of(context).unfocus();
     FirebaseUser user = (await _auth.signInWithEmailAndPassword(
          email: emailController.text.trim(),
          password: passwordController.text.trim()
      )).user;
      HashMap<String,dynamic> userData = await getUserData();
      await assignUserData(userData, context, true);
      if(user != null){
        Navigator.pushReplacementNamed(context, 'balance');
      }
    } catch (e){
      print(e);
      setState(() {
        print(errorWithLogin);
        emailController.clear();
        passwordController.clear();
        errorWithLogin = true;
      });
    }
  }

  resetPassword(String email) async {
    await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(
          "Reset link sent.",
          style: TextStyle(
              color: Colors.white
          ),
        ),
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
    Navigator.pop(context);
  }

}

