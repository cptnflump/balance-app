import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/project.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/alerts/project_alerts/add_project_modal.dart';
import 'package:flutter/material.dart';

class ProjectOptionsModal extends StatefulWidget {

  final String project;

  ProjectOptionsModal({@required this.project});

  @override
  _ProjectOptionsModalState createState() => _ProjectOptionsModalState();
}

class _ProjectOptionsModalState extends State<ProjectOptionsModal> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.02),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlatButton(
                child: AutoSizeText(
                  "DELETE",
                  style: TextStyle(
                      fontSize: kMainTextSize,
                      color: Colors.red),
                ),
                onPressed: (){
                  removeItem();
                  currProject = null;
                },
              ),
              FlatButton(
                child: AutoSizeText(
                  "RENAME",
                  style: TextStyle(
                    fontSize: kMainTextSize,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                onPressed: (){
                showEditProjectModal();
                },
              ),
            ],
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
        ],
      ),
    );
  }

  void removeItem() {
    userProjects.remove(widget.project);
    editTasks(widget.project);
    updateProjects();
      if(listFilter == widget.project){
        isFilteredByProject = false;
        listFilter = null;
        filteredUserTasks = userTasks;
        Navigator.pushReplacementNamed(context,'tasks');
      } else {
        Navigator.pop(context);
      }
  }

  editTasks(String project){
    userTasks.forEach((element) {
      if(element.project == widget.project){
        element.project = null;
      }
    });
    updateTasks();
  }

  void showEditProjectModal() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: const Radius.circular(10), topRight: const Radius.circular(10)),
        ),
        context: context,
        builder: (context) => ProjectModal(project: widget.project,));
    Navigator.pop(context);
    setState(() {});
  }
}
