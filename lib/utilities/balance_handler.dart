import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/alerts/check_in_alerts/check_in_confirmation_alert.dart';
import 'package:balance_app/widgets/alerts/confirmation_dialog.dart';
import 'package:flutter/material.dart';

int calculateBalance(){
  if(userBalance == null) return 50;
  if(userNumberCompletedBalance == 0 && userNumberCompletedTasks == 0){
    return 50;
  }
  int total = userNumberCompletedTasks + userNumberCompletedBalance;
  userBalance = ((userNumberCompletedBalance / total) * 100);
  return userBalance.round();
}

Color getBalanceBarColour(BuildContext context){
  if(userBalance == null) return Theme.of(context).primaryColor;
  if(userBalance < userGoal - 20){
    return kDarkRed;
  }
  if(userBalance < userGoal - 5){
    return kLightRed;
  }  else if(userBalance > userGoal + 20){
    return kLightGreen;
  } else if(userBalance > userGoal + 5){
    return Color(0xFFEECF6D);
  } else {
    return Theme.of(context).primaryColor;
  }
}

adjustBalance({int happiness, BuildContext context}){
  Navigator.pop(context);
  showDialog(
      context: context,
      builder: (context) => CheckInConfirmationAlert(
        happiness: happiness,
      )
  );

}