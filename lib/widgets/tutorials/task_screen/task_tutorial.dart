import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class TaskTutorial extends StatelessWidget {
  final Function onTap;

  TaskTutorial({
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        SizedBox(
          height: height * 0.25,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: Card(
            elevation: 4.0,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Theme.of(context).primaryColor, width: 1.5),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: width * 0.025, vertical: height * 0.02),
              child: Column(
                children: [
                  Text(
                    "The Task Screen",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kMainTextSize,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "Tap the plus button to create a new task.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "Open the menu in the top left to create and filter by project, as well as filter by due dates, priority or rest tasks.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "Pull down to refresh your tasks.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  GestureDetector(
                    onTap: onTap,
                    child: Text(
                      "Okay",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: kMainTextSize
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
