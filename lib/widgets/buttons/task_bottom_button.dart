import 'dart:math';

import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class TaskBottomButton extends StatelessWidget {
  final double screenHeight;
  final double screenWidth;
  final Function onTap;
  final IconData icon;
  final double sizeRatio;
  final bool hideCircle;

  TaskBottomButton({
    @required this.screenHeight,
    @required this.screenWidth,
    @required this.onTap,
    @required this.icon,
    this.sizeRatio=1.0,
    this.hideCircle=false,
  });

  @override
  Widget build(BuildContext context) {

    double smallestDimension = min(screenWidth,screenHeight);

    return RawMaterialButton(
      splashColor: hideCircle ? Colors.transparent : Theme.of(context).primaryColor,
      onPressed: onTap,
      elevation: hideCircle ? 0.0 : 5.0,
      fillColor: hideCircle ? Colors.transparent : Colors.white,
      child: Icon(
        icon,
        color: Theme.of(context).primaryColor,
        size: (smallestDimension * 0.1) * sizeRatio,
      ),
      padding: EdgeInsets.all(smallestDimension * 0.03),
      shape: CircleBorder(),
    );
  }
}
