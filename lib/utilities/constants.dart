import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//FILTER CONSTANTS
const priorityFilter = "priorityCONST";
const restFilter = "restCONST";
const dueTodayFilter = "todayCONST";
const dueInAWeekFilter = "inAWeekCONST";

//REVENUCAT CONSTANTS
const kRevenueCatPublicKey = "idbBUFRxpIqezzHlOmfHkmWKxSUDtcCb";

//TEXT CONSTANTS
const double kHugeTextSize = 30;
const double kBigTextSize = 26;
const double kTitleTextSize = 22;
const double kMainTextSize = 18;
const double kSmallTextSize = 16;
const double kSmallerTextSize = 14;
const double kSmallestTextSize = 12;

const TextStyle kAlertErrorTextStyle = TextStyle(
    color: Colors.red,
);

//ALERT CONSTANTS
const double kAlertBorderRadius = 20;

//COLOUR CONSTANTS
const Color kGreyWhite = Color(0xFFE0E2DF);
const Color kGrey = Color(0xFF535051);
const Color kOrange = Color(0xFFFF9D5C);
const Color kLightOrange = Color(0xFFFFA970);
const Color kButtonPressOrange = Color(0xFFFFC299);
const Color kDarkOrange = Color(0xFFFF781F);
const Color kLightRed = Color(0xFFCC5200);
const Color kDarkRed = Color(0xFFC13217);
const Color kMustard = Color(0xFFEECF6D);
const Color kLightGreen = Color(0xFF69DD79);
