import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/image_widgets/loading_circle.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ChangePasswordScreen extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  ChangePasswordScreen(this.scaffoldKey);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState(scaffoldKey);
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final _formKey = GlobalKey<FormState>();
  String errorMessage;
  Future<bool> canChangePassword;
  bool forgottenPassword = false;

  _ChangePasswordScreenState(this.scaffoldKey);

  @override
  void initState() {
    super.initState();
    canChangePassword = checkCanChangePassword();
  }

  @override
  Widget build(BuildContext context) {

    TextEditingController oldPassController = TextEditingController();
    TextEditingController newPassController = TextEditingController();


    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return FutureBuilder(
      future: canChangePassword,
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.done){
          if(snapshot.hasError){
            return Scaffold(body: Container(),);
          } else {
            print(snapshot.data);
            if(snapshot.data == true){
              return Scaffold(
                appBar: AppBar(
                  backgroundColor: Theme.of(context).primaryColor,
                  iconTheme: IconThemeData(
                    color: Colors.white,
                  ),
                  title: Text(
                    "Change Password",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: kTitleTextSize
                    ),
                  ),
                ),
                body: SafeArea(
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: height * 0.02,),
                          Text(
                            forgottenPassword ? "Reset Your Password" : "Change Your Password",
                            style: TextStyle(
                fontSize: kTitleTextSize,
                color: Theme.of(context).accentColor,
              ),
                          ),
                          Visibility(
                            visible: errorMessage != null,
                            child: AutoSizeText(
                              errorMessage == null ? "" : errorMessage,
                              style: TextStyle(color: Colors.red, fontSize: kSmallerTextSize),
                            ),
                          ),
                          SizedBox(height: height * 0.02,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AutoSizeText(
                                forgottenPassword ? "Email Address" : "Current Password",
                                style: TextStyle(
                                  fontSize: kSmallerTextSize,
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                              TextFormField(
                                cursorColor: Theme.of(context).primaryColor,
                                style: TextStyle(fontSize: kSmallerTextSize, color: Theme.of(context).accentColor),
                                autofocus: true,
                                decoration: InputDecoration(
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: kOrange),
                                  ),
                                ),
                                obscureText: !forgottenPassword,
                                controller: oldPassController,
                                validator: (value) {
                                  if(value.isEmpty){
                                    return "Please Enter Some Text";
                                  } else return null;
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: height * 0.02,),
                          Visibility(
                            visible: !forgottenPassword,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                AutoSizeText(
                                  "New Password",
                                  style: TextStyle(
                                    fontSize: kSmallerTextSize,
                                    color: Theme.of(context).accentColor,
                                  ),
                                ),
                                TextFormField(
                                  cursorColor: Theme.of(context).primaryColor,
                                  style: TextStyle(fontSize: kSmallerTextSize, color: Theme.of(context).accentColor),
                                  controller: newPassController,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: kOrange),
                                    ),
                                  ),
                                  validator: (value) {
                                    if(value.isEmpty){
                                      return "Please Enter Some Text";
                                    } else return null;
                                  },
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: height *0.01,),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: FlatButton(
                              splashColor: Theme.of(context).primaryColor.withOpacity(0.5),
                              child: Text(
                                forgottenPassword ? "Get Reset Link" : "Save",
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: kSmallTextSize
                                ),
                              ),
                              onPressed: (){
                                if(_formKey.currentState.validate()){
                                  String oldPass = oldPassController.text.trim();
                                  String newPass = newPassController.text.trim();
                                  forgottenPassword ? resetPassword(oldPass) :
                                  reAuthAndChangePassword(context: context, oldPass: oldPass, newPass: newPass);
                                }
                              },
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: FlatButton(
                              onPressed: (){
                                forgottenPassword = !forgottenPassword;
                                oldPassController.clear();
                                errorMessage = null;
                                setState(() {});
                              },
                              child: Text(
                                forgottenPassword ? "Change your password" : "Get a Password Reset Link",
                                style: TextStyle(
                                  fontSize: kSmallerTextSize,
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            } else {
              return Scaffold(
                body: SafeArea(
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                      child: Text(
                        "You authenticate with a third party account (such as Google), so cannot change your password.",
                        style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              );
            }
          }
        } else {
          return Scaffold(body: SafeArea(child: Center(child: LoadingCircle(width: width,),)),);
        }
      },
    );
  }

  /// This returns a string which is an error message, if the error message is null there was no error.
  reAuthAndChangePassword({BuildContext context, String oldPass, String newPass}) async{

//    DO REAUTH
    errorMessage = null;
    if(oldPass == newPass){
      errorMessage = "Current and new password cannot match.";
      setState(() {});
    } else{
      AuthCredential credential = EmailAuthProvider.getCredential(
          email: userEmail,
          password: oldPass
      );

     FirebaseUser user = await await FirebaseAuth.instance.currentUser();
      await user.reauthenticateWithCredential(credential).catchError((error){
        print(error);
        errorMessage = "Current Password Does Not Match.";
      });

      if(errorMessage == null){
        try{
          await user.updatePassword(newPass);
          Navigator.pop(context);
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Password Changed", style: TextStyle(color: Colors.white),), backgroundColor: Theme.of(context).primaryColor,));
        } catch (error) {
          print(error);
          errorMessage = "New Password is not valid.";
          setState(() {});
        }
      } else {
        setState(() {

        });
      }
    }

  }

  resetPassword(String email) async {
    await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(
          "Reset link sent.",
          style: TextStyle(
              color: Colors.white
          ),
        ),
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
    Navigator.pop(context);
  }

  Future<bool> checkCanChangePassword() async{
    bool canChange = false;
   FirebaseUser user = await await FirebaseAuth.instance.currentUser();
    user.providerData.forEach((element) {
      if(element.providerId == "password"){
        canChange = true;
      }
    });
    return canChange;
  }

}
