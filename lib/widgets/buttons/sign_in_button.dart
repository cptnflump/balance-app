import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class SignInButton extends StatelessWidget {

  final Widget child;
  final Function onTap;

  SignInButton({
    @required this.child,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {

    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).brightness == Brightness.dark ? Colors.black : Colors.white,
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: screenHeight * 0.02, horizontal:  screenWidth * 0.05),
          child: child,
        ),
      ),
      onTap: onTap,
    );
  }
}
