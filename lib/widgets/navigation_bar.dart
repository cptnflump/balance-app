import 'package:balance_app/screens/balance_screen.dart';
import 'package:balance_app/screens/profile_screen.dart';
import 'package:balance_app/screens/task_screen.dart';
import 'package:balance_app/widgets/page_transitions.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'image_widgets/sized_icon.dart';

class NavigationBar extends StatelessWidget {
  final bool disableBalance;
  final bool disableTask;
  final bool disableProfile;

  NavigationBar({
    this.disableBalance=false,
    this.disableTask=false,
    this.disableProfile=false,
});



  @override
  Widget build(BuildContext context) {
    double appBarHeight =
      MediaQuery.of(context).orientation == Orientation.portrait ?
      MediaQuery.of(context).size.height * 0.07 :
      MediaQuery.of(context).size.height * 0.1;
    double appBarWidth = MediaQuery.of(context).size.width;

    return BottomAppBar(
      elevation: 1,
      child: Container(
        height: appBarHeight,
        width: appBarWidth,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                child: Container(
                  color: Colors.transparent,
                  child: SizedIcon(
                      icon: FontAwesomeIcons.yinYang,
                      colour: disableBalance ? Theme.of(context).primaryColor : Theme.of(context).accentColor.withOpacity(0.5),
                    parentChildRatio: 0.5,
                  ),
                ),
                onTap: (){
                  if(!disableBalance) Navigator.pushReplacement(context, SlideRight(page: BalanceScreen()));
                  if(disableTask) updateCompleted();
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015),
              child: VerticalDivider(
                color: Theme.of(context).accentColor.withOpacity(0.5),
                thickness: 1,
              ),
            ),
            Expanded(
              child: GestureDetector(
                child: Container(
                  color: Colors.transparent,
                  child: SizedIcon(
                    icon: FontAwesomeIcons.solidCheckSquare,
                    colour: disableTask ? Theme.of(context).primaryColor : Theme.of(context).accentColor.withOpacity(0.5),
                    parentChildRatio: 0.5,
                  ),
                ),
                onTap: (){
                  if(!disableTask) Navigator.pushReplacement(context, SlideRight(page: TaskScreen()));
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015),
              child: VerticalDivider(
                color: Theme.of(context).accentColor.withOpacity(0.5),
                thickness: 1,
              ),
            ),
            Expanded(
              child: GestureDetector(
                child: Container(
                  color: Colors.transparent,
                  child: SizedIcon(
                    icon: FontAwesomeIcons.userAlt,
                    colour: disableProfile ? Theme.of(context).primaryColor : Theme.of(context).accentColor.withOpacity(0.5),
                    parentChildRatio: 0.5,
                  ),
                ),
                onTap: (){
                  if(!disableProfile) Navigator.pushReplacement(context, SlideLeft(page: ProfileScreen()));
                  if(disableTask) updateCompleted();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
