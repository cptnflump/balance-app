import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class InvalidChart extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AutoSizeText(
          "Hold Tight! I need a little more information before I can make beautiful graphs for you.\nKeep checking in!",
          style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: kSmallerTextSize
          ),
        ),
        Image.asset('images/chart-placeholder.png'),
      ],
    );
  }
}