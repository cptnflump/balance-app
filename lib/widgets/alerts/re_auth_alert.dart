import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ReAuthAlert extends StatefulWidget {
  @override
  _ReAuthAlertState createState() => _ReAuthAlertState();
}

class _ReAuthAlertState extends State<ReAuthAlert> {

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool errorWithLogin = false;
  String errorText = "Looks like you've entered incorrect details. Please try again.";

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Center(
      child: SingleChildScrollView(
        child: AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kAlertBorderRadius),
          ),
          title: Text("Enter Your Details"),
          content: ConstrainedBox(
            constraints: BoxConstraints(minWidth: width * 0.9, maxWidth:  width * 0.9 ),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Visibility(
                    visible: errorWithLogin,
                    child: Text(
                      errorText,
                      style: kAlertErrorTextStyle,
                    ),
                  ),
                  AutoSizeText(
                    "Please enter your details to delete your account.",
                    maxLines: 3,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: screenHeight * 0.03),
                    child: TextFormField(
                      autofocus: false,
                      cursorColor: Theme.of(context).primaryColor,
                      controller: emailController,
                      decoration: InputDecoration(
                        hintText: "Email Address",
                        labelStyle: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                      validator: (value) {
                        value = value.trim();
                        if (value.isEmpty || EmailValidator.validate(value) == false) {
                          return "Please enter a valid email address";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: screenHeight * 0.03),
                    child: TextFormField(
                      obscureText: true,
                      cursorColor: Theme.of(context).primaryColor,
                      controller: passwordController,
                      decoration: InputDecoration(
                        hintText: "Password",
                        labelStyle: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter some text";
                        }
                        return null;
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                "Delete Account",
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: kMainTextSize
                ),
              ),
              onPressed: (){
                if(_formKey.currentState.validate()){
                  try{
                    String email = emailController.text.trim();
                    String password = passwordController.text.trim();
                    reAuthAndDelete(email, password, context);
                  } catch (e){
                    print("ERROR");
                    print(e);
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

reAuthAndDelete(String email, String password, BuildContext context) async {
  AuthCredential credential = EmailAuthProvider.getCredential(
      email: email,
      password: password
  );

 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  await user.reauthenticateWithCredential(credential);
  deleteAccount(context);
}
