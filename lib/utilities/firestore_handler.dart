import 'dart:collection';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_constants.dart';
import 'package:balance_app/utilities/task.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

query({String collection, String document, String field}) async {
  var ds = await Firestore.instance
      .collection(collection)
      .document(document)
      .get();

  return ds[field];
}

Future<Map<String,dynamic>> getUserData() async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  var userData = await Firestore.instance.collection(fbUsersTable).document(
      user.uid).get();
  if (userData.data == null) {
    return null;
  } else {
    return HashMap.from(userData.data);
  }
}

Future<bool> forcedSubscriber() async {
  FirebaseUser user = await FirebaseAuth.instance.currentUser();
  DocumentSnapshot ds = await Firestore.instance.collection(fbUsersTable).document(user.uid).get();
  return ds[fbSubscriber];
}

incrementTaskNumber({bool isBalance}) async{
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  if(isBalance){
    userNumberCompletedBalance++;
    Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserCompletedBalance : userNumberCompletedBalance});
    userLifetimeCompletedBalance++;
    Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbLifeTimeBalanceTasks : userLifetimeCompletedBalance});
  } else {
    userNumberCompletedTasks++;
    Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserCompletedTasks : userNumberCompletedTasks});
    userLifetimeCompletedTasks++;
    Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbLifetimeCompletedTasks : userLifetimeCompletedTasks});
  }

}

getCheckIns() async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  var checkInData = await Firestore.instance.collection(fbCheckInTable).document(user.uid).get();
  if(checkInData.data == null){
    return null;
  } else {
    return HashMap.from(checkInData.data);
  }
}

resetTaskTotals() async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbLifetimeCompletedTasks : 0});
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbLifeTimeBalanceTasks : 0});
}

updateCheckInList(int happiness) async{
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  DateTime checkInDate = DateTime.now();
  Map<String,dynamic> checkIn = {};
  checkIn[checkInDate.toIso8601String()] = {
    fbCheckInDate : checkInDate,
    fbCheckInBalancePercentage : userBalance.round(),
    fbCheckInTasksCompleted : userNumberCompletedTasks,
    fbCheckInBalanceCompleted : userNumberCompletedBalance,
    fbCheckInGoal : userGoal.round(),
    fbCheckInHappiness : happiness,
  };
  Firestore.instance.collection(fbCheckInTable).document(user.uid).setData({fbCheckInList : checkIn}, merge: true);
}

saveTask(Task task) async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  Map<String, dynamic> taskStore = {};
  Map<String, dynamic> taskData = {};
  String entryTitle = task.taskTitle + "<SEP>" + DateTime.now().toIso8601String();
  taskData[entryTitle] = {
    fbTaskDue : task.taskDue,
    fbIsPriority : task.isPriority,
    fbIsBalance : task.isBalance,
    fbIsComplete : task.isComplete,
    fbTaskProject : task.project,
  };
  taskStore["tasks"] = taskData;
  Firestore.instance.collection(fbUsersTable).document(user.uid).setData(taskStore, merge: true);
  if(task.isComplete) Firestore.instance.collection(fbCompletedTasksTable).document(user.uid).setData(taskStore, merge: true);
}

updateTasks() async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  Map<String,dynamic> tasks = {};


  userTasks.forEach((element) {
    String entryTitle = element.taskTitle + "<SEP>" + DateTime.now().toIso8601String();
    tasks[entryTitle] = {
      fbIsComplete : element.isComplete,
      fbTaskDue : element.taskDue,
      fbIsBalance : element.isBalance,
      fbIsPriority : element.isPriority,
      fbTaskProject : element.project,
    };
  });
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({"tasks" : tasks});
  Firestore.instance.collection(fbUsersTable).document(user.uid).setData({fbUserCompletedTasks : userNumberCompletedTasks}, merge: true);
}

updateProjects() async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserProjects : userProjects});
 }

 updateGoal() async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserGoal : userGoal});
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserCompletedTasks : 0});
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserCompletedBalance : 0});
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserLastCheckIn : DateTime.now()});
 }

 finishBalanceTutorial() async {
  FirebaseUser user = await FirebaseAuth.instance.currentUser();
   Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserHasSeenBalance : true});
 }

 finishTaskTutorial() async {
  FirebaseUser user = await FirebaseAuth.instance.currentUser();
   Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserHasSeenTask : true});
 }

 finishCreateTaskTutorial() async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserHasCreatedTask : true});
}

resetTutorials() async{
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserHasCreatedTask : false});
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserHasSeenTask : false});
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbUserHasSeenBalance : false});
}

createUserEntry(FirebaseUser user, String givenName, String familyName) async {
  DocumentSnapshot ds = await Firestore.instance.collection(fbUsersTable).document(user.uid).get();
  if(ds.data == null){
    CollectionReference userRef = Firestore.instance.collection(fbUsersTable);
    await userRef.document(user.uid).setData(<String,dynamic>{
      fbUid : user.uid,
      fbTasksField : {},
      fbUserProjects : [],
      fbSubscriber : false,
      fbJoinedOn : DateTime.now(),
      fbUserEmail : user.email,
      fbGivenName : givenName,
      fbFamilyName : familyName,
      fbUserGoal : 50.0,
      fbUserBalance : 50.0,
      fbUserCompletedTasks : 0,
      fbUserCompletedBalance : 0,
      fbUserLastCheckIn : null,
      fbLifeTimeBalanceTasks : 0,
      fbLifetimeCompletedTasks : 0,
      fbUserHasSeenTask : false,
      fbUserHasSeenBalance : false,
      fbUserHasCreatedTask : false,
      fbLastAppOpen : null,
    });
  }
  ds = await Firestore.instance.collection(fbCompletedTasksTable).document(user.uid).get();
  if(ds.data == null){
    CollectionReference taskRef = Firestore.instance.collection(fbCompletedTasksTable);
    await taskRef.document(user.uid).setData({fbTasksField : {}});
  }
  ds = await Firestore.instance.collection(fbCheckInTable).document(user.uid).get();
  if(ds.data == null){
    CollectionReference taskRef = Firestore.instance.collection(fbCheckInTable);
    await taskRef.document(user.uid).setData({"$fbCheckInList" : {}});
  }
}

logAppOpen() async{
  FirebaseUser user = await FirebaseAuth.instance.currentUser();
  Firestore.instance.collection(fbUsersTable).document(user.uid).updateData({fbLastAppOpen : DateTime.now()});
}

deleteUserEntry() async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  await Firestore.instance.collection(fbUsersTable).document(user.uid).delete();
  await Firestore.instance.collection(fbCompletedTasksTable).document(user.uid).delete();
}