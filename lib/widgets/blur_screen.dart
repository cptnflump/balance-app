import 'dart:ui';

import 'package:flutter/material.dart';

class BlurScreen extends StatelessWidget {
  final Widget child;

  BlurScreen({
    this.child,
});

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Positioned.fill(
      child: ClipRect(
        child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
            child: child != null ? child : Container(width: width * 0.01, height: height * 0.01, color: Colors.transparent,)
        ),
      ),
    );
  }
}
