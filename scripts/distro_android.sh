#!/bin/bash
#This script build a new android APK and uploads it to firebase for distribution

distribute(){
  echo "Creating APK"

flutter build apk --split-per-abi

echo "APK created"

mv build/app/outputs/flutter-apk/app-armeabi-v7a-release.apk android_release.apk

echo "APK moved to root"

firebase appdistribution:distribute android_release.apk --app 1:700364450072:android:04392b7588d9aafca0166d --release-notes-file "android_distribution/android_release_notes.txt" --testers-file "android_distribution/android_testers.txt"

echo "Distribtution to firebase complete"

rm android_release.apk

echo "cleanup complete"
}

while true; do
    read -p "Have you updated the release notes and build number?" yn
    case $yn in
        [Yy]* ) distribute; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done


