import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/checkIn.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/LifetimeTaskInfo.dart';
import 'package:balance_app/widgets/charts/balance_bar_chart.dart';
import 'package:balance_app/widgets/charts/invalid_chart.dart';
import 'package:balance_app/widgets/generic_card.dart';
import 'package:balance_app/widgets/image_widgets/loading_circle.dart';
import 'package:balance_app/widgets/navigation_bar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ProgressScreen extends StatefulWidget {
  @override
  _ProgressScreenState createState() => _ProgressScreenState();
}

class _ProgressScreenState extends State<ProgressScreen> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    Future<List> checkIns = loadCheckIns();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Progress",
          style: TextStyle(color: Colors.white, fontSize: kTitleTextSize),
        ),
      ),
      bottomNavigationBar: NavigationBar(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: height * 0.05),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                child: AutoSizeText(
                  "How have you been doing?",
                  maxLines: 1,
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontSize: kTitleTextSize,
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.05,
              ),
              FutureBuilder(
                future: checkIns,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                          print(snapshot.error);
                      return InvalidChart();
                    } else {
                      return Align(
                          child: Container(
                              width: width * 0.9,
                              child: BalanceBarChart(
                                checkInData: snapshot.data,
                                chartData: ChartData.balance,
                              )));
                    }
                  } else {
                    return LoadingCircle(
                      width: width,
                    );
                  }
                },
              ),
              Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: height * 0.05,
                      ),
                      FutureBuilder(
                        future: checkIns,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.hasError)
                                {
                              return InvalidChart();
                            } else {
                              return Align(
                                  child: Container(
                                      width: width * 0.9,
                                      child: BalanceBarChart(
                                        checkInData: snapshot.data,
                                        chartData: ChartData.happiness,
                                      )));
                            }
                          } else {
                            return LoadingCircle(width: width);
                          }
                        },
                      ),
                      Visibility(
                        visible: userIsSubscriber,
                        child: Column(
                          children: [
                            SizedBox(
                              height: height * 0.05,
                            ),
                            FutureBuilder(
                              future: checkIns,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.done) {
                                  if (snapshot.hasError) {
                                    return InvalidChart();
                                  } else {
                                    return Align(
                                      child: Container(
                                        width: width * 0.9,
                                        child: BalanceBarChart(
                                          checkInData: snapshot.data,
                                          chartData: ChartData.numberWorkTasks,
                                        ),
                                      ),
                                    );
                                  }
                                } else {
                                  return LoadingCircle(width: width);
                                }
                              },
                            ),
                            SizedBox(
                              height: height * 0.05,
                            ),
                            FutureBuilder(
                              future: checkIns,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.done) {
                                  if (snapshot.hasError)
                                      {
                                    return InvalidChart();
                                  } else {
                                    return Align(
                                        child: Container(
                                            width: width * 0.9,
                                            child: BalanceBarChart(
                                              checkInData: snapshot.data,
                                              chartData:
                                                  ChartData.numberBalanceTasks,
                                            )));
                                  }
                                } else {
                                  return LoadingCircle(width: width);
                                }
                              },
                            ),
                            SizedBox(
                              height: height * 0.05,
                            ),
                            GenericCard(
                              elevation: 4,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: height * 0.01),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * 0.05),
                                  child: Column(
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        child: AutoSizeText(
                                          "What Makes You Happy",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: kTitleTextSize,
                                            color:
                                                Theme.of(context).accentColor,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: height * 0.03,
                                      ),
                                      FutureBuilder(
                                        future: checkIns,
                                        builder: (context, snapshot) {
                                          if (snapshot.connectionState ==
                                              ConnectionState.done) {
                                            if (snapshot.hasError) {
                                              return AutoSizeText(
                                                "I've has some trouble getting your data.",
                                                style: TextStyle(
                                                  fontSize: kSmallTextSize,
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                ),
                                              );
                                            } else {
                                              if((snapshot.data as List).isEmpty || getHighestAverageHappiness(snapshot.data) == 0){
                                                return AutoSizeText(
                                                  "Come back after a few check ins and see what's new!",
                                                  style: TextStyle(
                                                    fontSize: kSmallTextSize,
                                                    color: Theme.of(context)
                                                        .accentColor,
                                                  ),
                                                );
                                              } else {
                                                return Column(
                                                  children: [
                                                    AutoSizeText(
                                                      "So far on average, a balance of " +
                                                          getHighestAverageHappiness(
                                                              snapshot.data)
                                                              .toString() +
                                                          "% makes you happiest.",
                                                      style: TextStyle(
                                                        fontSize: kSmallTextSize,
                                                        color: Theme.of(context)
                                                            .accentColor,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: height * 0.03,
                                                    ),
                                                    AutoSizeText(
                                                      "Your perfect balance can change over time or even day to day, but this is a solid number to strive for!",
                                                      style: TextStyle(
                                                        fontSize: kSmallTextSize,
                                                        color: Theme.of(context)
                                                            .accentColor,
                                                      ),
                                                    ),
                                                  ],
                                                );
                                              }
                                            }
                                          } else {
                                            return LoadingCircle(
                                              width: width,
                                            );
                                          }
                                        },
                                      ),
                                      //todo here calculate the average balance that makes you happiest
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.05,
                            ),
                            GenericCard(
                              elevation: 4,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: height * 0.01),
                                child: Column(
                                  children: [
                                    Padding(
                                      padding:
                                          EdgeInsets.only(left: width * 0.05),
                                      child: Container(
                                        width: double.infinity,
                                        child: AutoSizeText(
                                          "Your Lifetime Stats",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              color:
                                                  Theme.of(context).accentColor,
                                              fontSize: kTitleTextSize),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: height * 0.02,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: width * 0.05),
                                      child: LifetimeTaskInfo(),
                                    ),
                                    SizedBox(
                                      height: height * 0.02,
                                    ),
                                    FutureBuilder(
                                      future: checkIns,
                                      builder: (context, snapshot) {
                                        if (snapshot.connectionState ==
                                            ConnectionState.done) {
                                          if (snapshot.hasError) {
                                            return SizedBox();
                                          } else {
                                            return Padding(
                                              padding: EdgeInsets.only(
                                                  left: width * 0.05),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      AutoSizeText(
                                                        "Average Happiness: ",
                                                        style: TextStyle(
                                                          fontSize:
                                                              kSmallTextSize,
                                                          color:
                                                              Theme.of(context)
                                                                  .accentColor,
                                                        ),
                                                      ),
                                                      AutoSizeText(
                                                        getAverageHappiness(
                                                                    snapshot
                                                                        .data) !=
                                                                0
                                                            ? (getAverageHappiness(
                                                                            snapshot.data) *
                                                                        20)
                                                                    .round()
                                                                    .toString() +
                                                                "%"
                                                            : "-",
                                                        style: TextStyle(
                                                          fontSize:
                                                              kSmallTextSize,
                                                          color:
                                                              Theme.of(context)
                                                                  .accentColor,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: height * 0.02,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      AutoSizeText(
                                                        "Average Balance: ",
                                                        style: TextStyle(
                                                          fontSize:
                                                              kSmallTextSize,
                                                          color:
                                                              Theme.of(context)
                                                                  .accentColor,
                                                        ),
                                                      ),
                                                      AutoSizeText(
                                                        getAverageBalance(
                                                                    snapshot
                                                                        .data) !=
                                                                0
                                                            ? getAverageBalance(
                                                                        snapshot
                                                                            .data)
                                                                    .toString() +
                                                                "%"
                                                            : "-",
                                                        style: TextStyle(
                                                          fontSize:
                                                              kSmallTextSize,
                                                          color:
                                                              Theme.of(context)
                                                                  .accentColor,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            );
                                          }
                                        } else {
                                          return LoadingCircle(
                                            width: width,
                                          );
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.05,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    visible: !userIsSubscriber,
                    child: Column(
                      children: [
                        Container(
                          width: width,
                          height: height * 0.25,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                            colors: [
                              Theme.of(context).brightness == Brightness.light
                                  ? Colors.white.withOpacity(0.1)
                                  : Color(0xFF121212).withOpacity(0.1),
                              Theme.of(context).brightness == Brightness.light
                                  ? Colors.white
                                  : Color(0xFF121212)
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                          )),
                        ),
                        Container(
                          color:
                              Theme.of(context).brightness == Brightness.light
                                  ? Colors.white
                                  : Color(0xFF121212),
                          width: width,
                          height: height * 0.5,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: height * 0.1,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * 0.1),
                                child: Text(
                                  "To see the rest of your stats, track your happiness and view your correlations upgrade to pro",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: kMainTextSize,
                                    color: Theme.of(context).accentColor,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: height * 0.05,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(context, 'goPro');
                                },
                                child: Text(
                                  "Go Pro",
                                  style: TextStyle(
                                      fontSize: kMainTextSize,
                                      color: Theme.of(context).primaryColor),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<List> loadCheckIns() async {
    List<CheckIn> checkIns = [];
    HashMap rawData = await getCheckIns();
    HashMap checkInData = HashMap.from(rawData[fbCheckInList]);
    if(checkInData.length > 0){
      checkInData.forEach((key, value) {
        CheckIn checkIn = CheckIn(
          checkInDate: (value[fbCheckInDate] as Timestamp).toDate(),
          balancePercent: value[fbCheckInBalancePercentage],
          balanceTasksCompleted: value[fbCheckInBalanceCompleted],
          goalBalance: value[fbCheckInGoal],
          workTasksCompleted: value[fbCheckInTasksCompleted],
          happiness: value[fbCheckInHappiness],
        );
        checkIns.add(checkIn);
      });
    }
    return checkIns;
  }

  double getAverageHappiness(List<CheckIn> checkIns) {
    int happinessOccurences = 0;
    int totalHappiness = 0;
    checkIns.forEach((element) {
      if (element.happiness != null) {
        totalHappiness += element.happiness;
        happinessOccurences++;
      }
    });
    if (totalHappiness == 0) {
      return 0;
    } else {
      return (totalHappiness / happinessOccurences);
    }
  }

  int getAverageBalance(List<CheckIn> checkIns) {
    int totalBalance = 0;
    checkIns.forEach((element) {
      totalBalance += element.balancePercent;
    });
    if (totalBalance == 0) {
      return 0;
    } else {
      return (totalBalance / checkIns.length).round();
    }
  }

  int getHighestAverageHappiness(List<CheckIn> checkIns) {
    //find the maximum happiness score achieved
    //collect all check ins with this score
    //average the balance percentage
    int maxHappiness = 0;
    int i = 0;
    while (i < checkIns.length && maxHappiness < 5) {
      if (checkIns[i].happiness > maxHappiness) {
        maxHappiness = checkIns[i].happiness;
      }
      i++;
    }
    if(maxHappiness < 4){
      return 0;
    } else {
      int totalBalance = 0;
      int numberHappiestCheckIns = 0;
      checkIns.forEach((element) {
        if (element.happiness == maxHappiness) {
          totalBalance += element.balancePercent;
          numberHappiestCheckIns++;
        }
      });
      return (totalBalance / numberHappiestCheckIns).round();
    }
    }
}
