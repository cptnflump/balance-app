import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/project.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/rendering.dart';


class ProjectListWidget extends StatefulWidget {

  @override
  _ProjectListWidgetState createState() => _ProjectListWidgetState();
}

class _ProjectListWidgetState extends State<ProjectListWidget> {

  double height;
  double width;
  List<String> projects;

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    projects = userProjects;

    return LayoutBuilder(
      builder: (context, constraints) => Container(
        height: constraints.maxHeight,
        width: constraints.maxWidth,
        child: ListView.builder(
        itemCount: userProjects.length,
        itemBuilder: _getListItem,
        ),
      ),
    );
  }


  Widget _getListItem(BuildContext context, int index){

    return GestureDetector(
      onTap: (){
        setState(() {
          currProject = userProjects[index];
          Navigator.pop(context);
        });
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                constraints: BoxConstraints(minWidth: width * 0.75, maxWidth:  width * 0.75 ),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: height * 0.01),
                  child: AutoSizeText(
                    projects[index],
                    style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
                    maxLines: 3,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: height * 0.01,),
          index == projects.length - 1 ? SizedBox() : Divider(
            thickness: 1.0,
            color: Theme.of(context).primaryColor,
          ),
        ],
      ),
    );
  }

  List<Widget> createChildren(){
    List<Widget> widgets = [];
    for(String projectData in projects){
      widgets.add(
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: AutoSizeText(
              projectData,
              style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
              maxLines: 1,
            ),
          )
      );
      widgets.add(
          Divider(
            thickness: 1.0,
            color: Theme.of(context).primaryColor,
          )
      );
    }
    widgets.removeLast();
    return widgets;
  }
}

