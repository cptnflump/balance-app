import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/time_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/alerts/project_alerts/add_project_modal.dart';
import 'package:balance_app/widgets/lists/project_list_drawer_widget.dart';
import 'package:flutter/material.dart';

class TaskScreenDrawer extends StatefulWidget {
  const TaskScreenDrawer({
    @required this.screenHeight,
    @required this.screenWidth,
  });

  final double screenHeight;
  final double screenWidth;

  @override
  _TaskScreenDrawerState createState() => _TaskScreenDrawerState();
}

class _TaskScreenDrawerState extends State<TaskScreenDrawer> {

  bool projectListVisibility = false;
  bool filterVisiblity = false;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: widget.screenHeight * 0.14,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(
                          color: Theme.of(context).primaryColor,  // Text colour here
                          width: 1.5, // Underline width
                        ),),
                      ),
                      child: AutoSizeText(
                        "Projects and Filters",
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: kTitleTextSize,
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                    ),
                    SizedBox(height: widget.screenHeight * 0.04,),
                  ],
                ),
              ),
              SizedBox(height: widget.screenHeight * 0.02,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: widget.screenWidth * 0.1),
                          child: Icon(Icons.format_list_bulleted, color: Theme.of(context).primaryColor, size: widget.screenWidth * 0.07,),
                        ),
                        AutoSizeText(
                          "Projects",
                          style: TextStyle(
                              fontSize: kMainTextSize,
                              color: Theme.of(context).accentColor
                          ),
                        ),
                      ],
                    ),
                    onTap: (){
                      setState(() {
                        projectListVisibility = !projectListVisibility;
                        if(projectListVisibility) filterVisiblity = false;
                      });
                    },
                  ),
                  Visibility(
                    visible: projectListVisibility,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: MediaQuery.of(context).size.height * 0.015),
                        Visibility(
                          visible: userProjects.length > 0,
                          child: LayoutBuilder(
                            builder: (context,constraints) => Container(
                              height: MediaQuery.of(context).size.height * 0.3,
                              width:  constraints.maxWidth * 0.9,
                              child: ProjectListDrawerWidget(),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            showProjectModal();
                          },
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: widget.screenWidth * 0.1),
                                child: Icon(Icons.add, color: Theme.of(context).primaryColor, size: widget.screenWidth * 0.07,),
                              ),
                              AutoSizeText(
                                "Create Project",
                                style: TextStyle(
                fontSize: kSmallTextSize,
                color: Theme.of(context).accentColor,
              ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: widget.screenHeight * 0.05,),
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        filterVisiblity = !filterVisiblity;
                        if(filterVisiblity) projectListVisibility = false;
                      });
                    },
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: widget.screenWidth * 0.1),
                          child: Icon(Icons.filter_list, color: Theme.of(context).primaryColor, size: widget.screenWidth * 0.07,),
                        ),
                        AutoSizeText(
                          "Filters",
                          style: TextStyle(
                              fontSize: kMainTextSize,
                              color: Theme.of(context).accentColor
                          ),
                        ),
                      ],
                    ),
                  ),
                  Center(
                    child: Visibility(
                      visible: filterVisiblity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: widget.screenHeight * 0.05,),
                          GestureDetector(
                            onTap: (){
                              isFilteredByProject = false;
                              listFilter = listFilter == priorityFilter ? null : priorityFilter;
                              filteredUserTasks = listFilter == null ? userTasks : List.of(userTasks.where((element) => element.isPriority));
                              Navigator.pushReplacementNamed(context,'tasks');
                            },
                            child: AutoSizeText(
                              "Filter by priority",
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: kSmallerTextSize,
                              ),
                            ),
                          ),
                          SizedBox(height: widget.screenHeight * 0.025,),
                          GestureDetector(
                            onTap: (){
                              isFilteredByProject = false;
                              listFilter = listFilter == restFilter ? null : restFilter;
                              filteredUserTasks = listFilter == null ? userTasks : List.of(userTasks.where((element) => element.isBalance));
                              Navigator.pushReplacementNamed(context,'tasks');
                            },
                            child: AutoSizeText(
                              "Filter by rest tasks",
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: kSmallerTextSize,
                              ),
                            ),
                          ),
                          SizedBox(height: widget.screenHeight * 0.025,),
                          GestureDetector(
                            onTap: (){
                              isFilteredByProject = false;
                              listFilter = listFilter == dueTodayFilter ? null : dueTodayFilter;
                              DateTime today = startOfDay(DateTime.now());
                              filteredUserTasks = listFilter == null ? userTasks : List.of(userTasks.where((element) => element.taskDue != null && element.taskDue.difference(today).inDays <= 0));
                              Navigator.pushReplacementNamed(context, 'tasks');
                            },
                            child: AutoSizeText(
                              "Tasks due today",
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: kSmallerTextSize,
                              ),
                            ),
                          ),
                          SizedBox(height: widget.screenHeight * 0.025,),

                          GestureDetector(
                            onTap: (){
                              isFilteredByProject = false;
                              listFilter = listFilter == dueInAWeekFilter ? null : dueInAWeekFilter;
                              DateTime today = startOfDay(DateTime.now());
                              filteredUserTasks = listFilter == null ? userTasks : List.of(userTasks.where((element) => element.taskDue != null && element.taskDue.difference(today).inDays <= 7));
                              Navigator.pushReplacementNamed(context, 'tasks');
                            },
                            child: AutoSizeText(
                              "Tasks due in the next 7 days",
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: kSmallerTextSize,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: widget.screenHeight * 0.05),
                      child: Container(
                          height: MediaQuery.of(context).size.height * 0.2,
                          child: AspectRatio(
                            aspectRatio: 3/2,
                              child: Image.asset(createTimedImageAsset(),
                              ),
                          ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  showProjectModal() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: const Radius.circular(10), topRight: const Radius.circular(10)),
        ),
        context: context,
        builder: (context) => ProjectModal()
    );
  }
}