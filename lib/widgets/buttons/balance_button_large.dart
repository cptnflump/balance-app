import 'dart:math';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/widgets/image_widgets/sized_icon.dart';
import 'package:flutter/material.dart';

class BalanceButtonLarge extends StatelessWidget {
  const BalanceButtonLarge({
    @required this.width,
    @required this.height,
    @required this.onTap,
    @required this.icon,
    @required this.text,
  });

  final double width;
  final double height;
  final Function onTap;
  final String text;
  final IconData icon;

  @override
  Widget build(BuildContext context) {

    double smallestDimension = min(height,width);

    return RawMaterialButton(
      splashColor: Theme.of(context).primaryColor,
      onPressed: onTap,
      elevation: 5.0,
      fillColor: Colors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: width * 0.2),
        child: Container(
          height: height * 0.18,
          child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    icon,
                    color: Theme.of(context).primaryColor,
                    size: smallestDimension * 0.07,
                  ),
                  SizedBox(height: height * 0.02,),
                  AutoSizeText(
                      text,
                      style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
                  ),
                ],
              ),
        ),
      ),
      padding: EdgeInsets.all(smallestDimension * 0.02),
      shape: CircleBorder(),
    );
  }
}