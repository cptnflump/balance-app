import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/screens/task_screen.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/project.dart';
import 'package:balance_app/utilities/task.dart';
import 'package:balance_app/utilities/time_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/alerts/project_alerts/project_dialog.dart';
import 'package:balance_app/widgets/buttons/date_button.dart';
import 'package:balance_app/widgets/buttons/project_button.dart';
import 'package:balance_app/widgets/tutorials/task_screen/options_tutorial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

final FocusNode taskTitleFocusNode = FocusNode();
class TaskModal extends StatefulWidget {
  final Task task;

  TaskModal({
    this.task,
  });

  @override
  _TaskModalState createState() => _TaskModalState();
}

class _TaskModalState extends State<TaskModal> {
  final controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  double width;
  double height;

  DateTime dueDate;
  String project;
  bool isPriority = false;
  bool isBalance = false;
  Color fireColour;
  Color lotusColour;
  Color calendarColour;
  Color tagColour;

  bool dateVisibility = false;
  bool projectVisibility = false;

  bool projectHasChanged = false;
  bool dueDateHasChanged = false;

  bool todayDatePicked;
  bool tomorrowDatePicked;
  bool customDatePicked;


  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    if(fireColour == null) fireColour = Theme.of(context).accentColor;
    if(lotusColour == null) lotusColour = Theme.of(context).accentColor;
    if(calendarColour == null) calendarColour = Theme.of(context).accentColor;
    if(tagColour == null) tagColour = Theme.of(context).accentColor;

    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;

    if (widget.task != null) {
      if(controller.text == "") controller.text = widget.task.taskTitle;
      if(widget.task.project != null){
        tagColour = Theme.of(context).primaryColor;
        if(!projectHasChanged){
          currProject = widget.task.project;
        }
      }
      if (widget.task.isPriority) {
        fireColour = Theme.of(context).primaryColor;
        isPriority = true;
      }
      if (widget.task.isBalance) {
        lotusColour = Theme.of(context).primaryColor;
        isBalance = true;
      }
      if(widget.task.taskDue != null){
        calendarColour = Theme.of(context).primaryColor;
        if(!dueDateHasChanged) dueDate = widget.task.taskDue;
      }
      if(widget.task.taskDue != null && todayDatePicked == null && tomorrowDatePicked == null && customDatePicked == null){configColours();
      } else {
        if(todayDatePicked == null) todayDatePicked = false;
        if(tomorrowDatePicked == null) tomorrowDatePicked = false;
        if(customDatePicked == null) customDatePicked = false;
      }
    } else {
      if(isFilteredByProject != null && isFilteredByProject){
        if(!projectHasChanged) {
          currProject = listFilter;
        }
      }

      if(todayDatePicked == null) todayDatePicked = false;
      if(tomorrowDatePicked == null) tomorrowDatePicked = false;
      if(customDatePicked == null) customDatePicked = false;
    }
    if(todayDatePicked == false && tomorrowDatePicked == false && customDatePicked == false){
      calendarColour = Theme.of(context).accentColor;
    } else {
      calendarColour = Theme.of(context).primaryColor;
    }
    if(currProject != null){
      tagColour = Theme.of(context).primaryColor;
    } else {
      tagColour = Theme.of(context).accentColor;
    }

    return Builder(
        builder: (context) => Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: Container(
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(
                          height: height * 0.02,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: TextFormField(
                                focusNode: taskTitleFocusNode,
                                textInputAction: TextInputAction.done,
                                onFieldSubmitted: (value) {
                                  validateAndUpdate();
                                },
                                textCapitalization: TextCapitalization.sentences,
                                cursorColor: Theme.of(context).primaryColor,
                                autofocus: widget.task == null && userHasCreatedTask ? true : false,
                                controller: controller,
                                decoration: InputDecoration.collapsed(
                                  hintText: "E.g catch up on emails",
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Please enter some text";
                                  }
                                  return null;
                                },
                              ),
                            ),
                            widget.task != null ? GestureDetector(
                              onTap: (){
                                validateAndUpdate();
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    child: Icon(
                                      Icons.done,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                  AutoSizeText("DONE", style: TextStyle(
                fontSize: kSmallerTextSize,
                color: Theme.of(context).accentColor,
              ),),
                                ],
                              ),
                            ) : SizedBox(),
                          ],
                        ),
                        SizedBox(height: height * 0.02,),
                        Row(
                          children: <Widget>[
                            SizedBox(width: width * 0.015,),
                            GestureDetector(
                              child: FaIcon(
                                FontAwesomeIcons.calendarAlt,
                                color: calendarColour,
                              ),
                              onTap: () {
                                setState(() {
                                  dateVisibility = !dateVisibility;
                                  projectVisibility = false;
                                });
                              },
                            ),
                            SizedBox(
                              width: width * 0.1,
                            ),
                            GestureDetector(
                              child: FaIcon(
                                FontAwesomeIcons.burn,
                                color: fireColour,
                              ),
                              onTap: () {
                                setState(() {
                                  isPriority = !isPriority;
                                  if (widget.task != null) widget.task.isPriority = isPriority;
                                  fireColour = isPriority ? Theme.of(context).primaryColor : Theme.of(context).accentColor;
                                });
                              },
                            ),
                            SizedBox(
                              width: width * 0.1,
                            ),
                            GestureDetector(
                              child: FaIcon(
                                FontAwesomeIcons.spa,
                                color: lotusColour,
                              ),
                              onTap: () {
                                setState(() {
                                  isBalance = !isBalance;
                                  if (widget.task != null) widget.task.isBalance = isBalance;
                                  lotusColour = isBalance ? Theme.of(context).primaryColor : Theme.of(context).accentColor;
                                });
                              },
                            ),
                            SizedBox(
                              width: width * 0.1,
                            ),
                            GestureDetector(
                              child: FaIcon(
                                FontAwesomeIcons.tag,
                                color: tagColour,
                              ),
                              onTap: () {
                                setState(() {
                                  projectVisibility = !projectVisibility;
                                  dateVisibility = false;
                                });
                              },
                            ),
                            SizedBox(width: width * 0.02,),
                            currProject == null ? SizedBox() : AutoSizeText(
                              currProject,
                              style: TextStyle(
                fontSize: kSmallerTextSize,
                color: Theme.of(context).accentColor,
              ),
                            ),
                          ],
                        ),
                        Visibility(
                          visible: dateVisibility,
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: height * 0.04,),
                              Row(
                                children: <Widget>[
                                  DateButton(
                                    width: width,
                                    boxColour: todayDatePicked ? Theme.of(context).primaryColor : Colors.transparent,
                                    text: "Today",
                                    onTap: (){
                                      setState(() {
                                        dueDateHasChanged = true;
                                        todayDatePicked = !todayDatePicked;
                                        tomorrowDatePicked = false;
                                        customDatePicked = false;
                                        if(todayDatePicked){
                                          dueDate = startOfDay(DateTime.now());
                                        } else {
                                          dueDate = null;
                                        }
                                      });
                                    },
                                  ),
                                  SizedBox(width: width * 0.05,),
                                  DateButton(
                                    width: width,
                                    boxColour: tomorrowDatePicked ? Theme.of(context).primaryColor : Colors.transparent,
                                    text: "Tomorrow",
                                    onTap: (){
                                      setState(() {
                                        dueDateHasChanged = true;
                                        tomorrowDatePicked = !tomorrowDatePicked;
                                        todayDatePicked = false;
                                        customDatePicked = false;
                                        if(tomorrowDatePicked){
                                          dueDate = startOfDay(DateTime.now()).add(Duration(days: 1));
                                        } else {
                                          dueDate = null;
                                        }
                                      });
                                    },
                                  ),
                                  SizedBox(width: width * 0.05,),
                                  DateButton(
                                    width: width,
                                    boxColour: customDatePicked ? Theme.of(context).primaryColor : Colors.transparent,
                                    text: "Pick Date",
                                    onTap: (){
                                      setState(() {
                                        dueDateHasChanged = true;
                                        customDatePicked = !customDatePicked;
                                        tomorrowDatePicked = false;
                                        todayDatePicked = false;
                                        if(customDatePicked){
                                          pickDate();
                                        } else {
                                          dueDate = null;
                                        }
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: projectVisibility,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(height: height * 0.04,),
                              Container(
                                height: height * 0.03,
                                width: width,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: createProjectList(),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: widget.task == null ? height * 0.02 : height * 0.05,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
        ));
  }

  List<Widget> createProjectList(){

    List<Widget> projects = [];
    userProjects.forEach((element) {
      projects.add(ProjectButton(
        width: width,
        boxColour: currProject == element ? Theme.of(context).primaryColor : Colors.transparent,
        text: element,
        onTap: (){
          setState(() {
            projectHasChanged = true;
            currProject == element ? currProject = null : currProject = element;
          });
        },
      ));
      projects.add(SizedBox(width: width * 0.05,));
    });
    return projects;
  }

  void validateAndUpdate() {
    if (_formKey.currentState.validate()) {
      Task task = Task(
        taskTitle: controller.text.trim(),
        taskDue: dueDate,
        isPriority: isPriority,
        isBalance: isBalance,
        isComplete: false,
        project: currProject,
      );

      if (widget.task == null) {
        if(listFilter != null) checkCurrFilter(task);
        userTasks.add(task);
        saveTask(task);
      } else {
        widget.task.taskTitle = controller.text;
        widget.task.isBalance = isBalance;
        widget.task.taskDue = dueDate;
        widget.task.project = currProject;
        widget.task.isPriority = isPriority;
        updateTasks();
      }
      currProject = null;
      Navigator.pop(context);
      userTasks = TaskListHandler().orderTasks();
      if(!userHasCreatedTask){
        showDialog(
            context: context,
          builder: (context) => TaskOptionsTutorial(
            onTap: (){
              Navigator.pop(context);
              userHasCreatedTask = true;
              finishCreateTaskTutorial();
            }
          )
        );
      }
    }
  }

  handleProject() async{
    if(currProject == null){
      await showDialog(
          context: context,
          builder: (context) => ProjectDialog());
    } else {
      currProject = null;
      tagColour = Theme.of(context).accentColor;
    }
    if(widget.task != null){
      widget.task.project = currProject;
    }
    setState(() {});
  }

  Future<Null> pickDate() async {
    DateTime selectedDate = dueDate;

    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now().add(Duration(days: -1)),
        lastDate: DateTime.now().add(Duration(days: 365)),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              primaryColor: Theme.of(context).primaryColor,
              accentColor: Theme.of(context).primaryColor,
              colorScheme: ColorScheme.light(primary: Theme.of(context).primaryColor),
              buttonTheme: ButtonThemeData(
                textTheme: ButtonTextTheme.primary,
              ),
            ),
            child: child,
          );
        });
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        calendarColour = Theme.of(context).primaryColor;
        dueDate = startOfDay(selectedDate);
      });
    if(picked == null){
      setState(() {
        customDatePicked = false;
      });
    }
  }

  void configColours(){
    if(widget.task == null || (widget.task != null && widget.task.taskDue == null)){
      todayDatePicked = false;
      tomorrowDatePicked = false;
      customDatePicked = false;
    } else {
      DateTime today = startOfDay(DateTime.now());
      int days = widget.task.taskDue
          .difference(today)
          .inDays;
      if(days == 0){
        todayDatePicked = true;
        tomorrowDatePicked = false;
        customDatePicked = false;
      } else if (days == 1){
        todayDatePicked = false;
        tomorrowDatePicked = true;
        customDatePicked = false;
      } else {
        todayDatePicked = false;
        tomorrowDatePicked = false;
        customDatePicked = true;
      }
    }
  }

  checkCurrFilter(Task task){
    if(isFilteredByProject){
      if(listFilter == task.project)filteredUserTasks.add(task);
      } else {
        DateTime today = startOfDay(DateTime.now());
        switch(listFilter){
          case dueTodayFilter:
            if(task.taskDue != null && task.taskDue.difference(today).inDays <= 0) filteredUserTasks.add(task);
            break;
          case dueInAWeekFilter:
            if(task.taskDue != null && task.taskDue.difference(today).inDays <= 7) filteredUserTasks.add(task);
            break;
          case priorityFilter:
            if(task.isPriority) filteredUserTasks.add(task);
            break;
          case restFilter:
            if(task.isBalance) filteredUserTasks.add(task);
            break;
        }
    }
  }
}





