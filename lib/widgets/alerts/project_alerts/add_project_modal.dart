import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:flutter/material.dart';

class ProjectModal extends StatefulWidget {

  final String project;

  ProjectModal({this.project});

  @override
  _ProjectModalState createState() => _ProjectModalState(this.project);
}

class _ProjectModalState extends State<ProjectModal> {

  _ProjectModalState(this.project);

  final TextEditingController controller = TextEditingController();
  final String project;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    if(widget.project != null){
      controller.text = widget.project;
    }

    return Builder(
      builder: (context) => Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: Container(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05, vertical: MediaQuery.of(context).size.height * 0.025),
            child: TextFormField(
              textInputAction: TextInputAction.done,
              onFieldSubmitted: (value) {
                if(widget.project == null){
                  userProjects.add(value.trim());
                } else {
                  userProjects[userProjects.indexOf(widget.project)] = value;
                  editTasks(widget.project);
                }
                updateProjects();
                Navigator.pop(context);
                setState(() {});
              },
              textCapitalization: TextCapitalization.sentences,
              cursorColor: Theme.of(context).primaryColor,
              autofocus: true,
              controller: controller,
              decoration: InputDecoration.collapsed(
                hintText: "Project Title",
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return "Please enter some text";
                }
                return null;
              },
            ),
          ),
        ),
      ),
    );
  }

  editTasks(String project){
    userTasks.forEach((element) {
      if(element.project == project){
        element.project = controller.text.trim();
      }
    });
    updateTasks();
  }

}
