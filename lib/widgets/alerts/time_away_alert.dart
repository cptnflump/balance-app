import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:flutter/material.dart';

class TimeAwayAlert extends StatefulWidget {

  final GlobalKey<ScaffoldState> scaffoldKey;

  TimeAwayAlert({@required this.scaffoldKey});

  @override
  _TimeAwayAlertState createState() => _TimeAwayAlertState(scaffoldKey);
}

class _TimeAwayAlertState extends State<TimeAwayAlert> {

  final GlobalKey<ScaffoldState> scaffoldKey;

  _TimeAwayAlertState(this.scaffoldKey);

  @override
  Widget build(BuildContext context) {

    int hoursSinceLastOpen = DateTime.now().difference(userLastAppOpen).inHours;

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: height * 0.03,),
          Text(
            "You've been away for $hoursSinceLastOpen hours.",
            style: TextStyle(
              fontSize: kSmallTextSize,
              color: Theme.of(context).accentColor,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: height * 0.05,),
          GestureDetector(
              child: Text(
                "Log it as work",
                style: TextStyle(
                  fontSize: kMainTextSize,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            onTap: (){
                for (int i = 0; i < hoursSinceLastOpen; i++) {
                  incrementTaskNumber(isBalance: false);
                }
                Navigator.pop(context);
                showSnackBar(height: height, text: "$hoursSinceLastOpen Work Tasks Logged");
                setState(() {});
            },
          ),
          SizedBox(height: height * 0.05,),
          GestureDetector(
            child: Text(
              "Log it as rest",
              style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).primaryColor,
              ),
            ),
            onTap: (){
              for (int i = 0; i < hoursSinceLastOpen; i++) {
                incrementTaskNumber(isBalance: true);
              }
              Navigator.pop(context);
              showSnackBar(height: height, text: "$hoursSinceLastOpen Rest Tasks Logged");
              setState(() {});
            },
          ),
          SizedBox(height: height * 0.05,),
          GestureDetector(
            child: Text(
              "I'll log it manually",
              style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).primaryColor,
              ),
            ),
            onTap: (){
              Navigator.pop(context);
            },
          ),
          SizedBox(height: height * 0.03,),
        ],
      ),
    );
  }

  showSnackBar({double height, String text}){
    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Theme.of(context).primaryColor,
        duration: Duration(seconds: 1),
        content: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: height * 0.05),
          child: AutoSizeText(
            text,
            maxLines: 1,
            style: TextStyle(color: Colors.white, fontSize: kMainTextSize),
          ),
        ),
      ),
    );
  }

}
