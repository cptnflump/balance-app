class CheckIn{
  int balancePercent;
  int goalBalance;
  int workTasksCompleted;
  int balanceTasksCompleted;
  DateTime checkInDate;
  int happiness;

  CheckIn({
    this.checkInDate,
    this.balancePercent,
    this.balanceTasksCompleted,
    this.goalBalance,
    this.workTasksCompleted,
    this.happiness,
  });

}