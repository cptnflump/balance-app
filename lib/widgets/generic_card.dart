import 'package:flutter/material.dart';

class GenericCard extends StatelessWidget {

  final Widget child;
  final double elevation;

  GenericCard({
    @required this.child,
    this.elevation=2.0
  });

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Align(
      child: Container(
        width: width * 0.9,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: elevation,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: height * 0.02),
            child: child,
          ),
        ),
      ),
    );
  }
}