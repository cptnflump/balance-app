import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/alerts/project_alerts/project_options_modal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/rendering.dart';


class ProjectListDrawerWidget extends StatefulWidget {

  @override
  _ProjectListDrawerWidgetState createState() => _ProjectListDrawerWidgetState();
}

class _ProjectListDrawerWidgetState extends State<ProjectListDrawerWidget> {

  double height;
  double width;
  List<String> projects;

  @override
  Widget build(BuildContext context) {

    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    projects = userProjects;
    projects.sort();

    return LayoutBuilder(
      builder: (context, constraints) => Container(
        height: constraints.maxHeight,
        width: constraints.maxWidth,
        child: ListView.builder(
          itemCount: userProjects.length,
          itemBuilder: _getListItem,
        ),
      ),
    );
  }


  Widget _getListItem(BuildContext context, int index){

    return GestureDetector(
      onLongPress: (){
        showProjectOptionModal(projects[index]);
      },
      onTap: (){
        listFilter = listFilter == projects[index] ? null : projects[index];
        isFilteredByProject = listFilter == projects[index];
        filteredUserTasks = listFilter == null ? userTasks : List.of(userTasks.where((element) => element.project == projects[index]));
        Navigator.pushReplacementNamed(context,'tasks');
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: height * 0.01),
              child: Center(
                child: AutoSizeText(
                  projects[index],
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontSize: kSmallerTextSize
                  ),
                  maxLines: 3,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void showProjectOptionModal(String project) async {
    await showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: const Radius.circular(10), topRight: const Radius.circular(10)),
        ),
        context: context,
        builder: (context) => ProjectOptionsModal(project: project,)
    );
    setState(() {});
  }

}

