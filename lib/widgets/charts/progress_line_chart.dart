import 'dart:math';

import 'package:balance_app/utilities/checkIn.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

enum ProgressLineChartData{
  balance,
  happiness,
}

class ProgressLineChart extends StatelessWidget {
  final List<Color> gradientColors = [];
  final List<CheckIn> checkInData;
  final ProgressLineChartData chartType;


  ProgressLineChart({this.checkInData, this.chartType});

  @override
  Widget build(BuildContext context) {
    gradientColors.add(Theme.of(context).primaryColor);

    checkInData.sort((a,b) => a.checkInDate.compareTo(b.checkInDate));
    int maxBalance = getMaxBalance();
    List<FlSpot> dataPoints = createDataPoints();

    return LineChart(
      LineChartData(
        minX: -0.1,
        maxX: checkInData.length.toDouble()-1,
        minY: getMinY(),
        maxY: getMaxY(),
        gridData: createGridData(context),
        titlesData: createTitleData(context, maxBalance),
        borderData: createBorderData(context),
        lineBarsData: createLineData(dataPoints),
      ),
    );
  }

  createGridData(BuildContext context){
    return FlGridData(
      show: true,
      drawVerticalLine: false,
      drawHorizontalLine: false,
      getDrawingHorizontalLine: (value) {
        if(value == 50){
          return FlLine(
            color: Theme.of(context).primaryColor.withOpacity(0.5),
            strokeWidth: 1,
          );
        } else return FlLine(color: Colors.transparent);
      },
      getDrawingVerticalLine: (value) {
        return FlLine(
          color: Theme.of(context).accentColor.withOpacity(0.5),
        );
      },
    );
  }

  createTitleData(BuildContext context, int maxBalance){
    return FlTitlesData(
      show: true,
      bottomTitles: SideTitles(
        showTitles: true,
        reservedSize: 20,
        textStyle: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 12
        ),
        getTitles: (value) {
          return writeBottomTitle(value);
        },
        margin: 10,
      ),
      leftTitles: SideTitles(
        showTitles: true,
        textStyle: TextStyle(
          color: Theme.of(context).accentColor,
          fontSize: chartType == ProgressLineChartData.balance ? 12 : 10,
        ),
        getTitles: (value) {
          return writeLeftTitle(value);
        },
        reservedSize: 25,
        margin: 10,
      ),
    );
  }

  createBorderData(BuildContext context){
    return FlBorderData(
        show: true,
        border: Border(
          bottom: BorderSide(color: Theme.of(context).primaryColor, width: 1),
          left: BorderSide(color: Theme.of(context).primaryColor, width: 1),
        ),
    );
  }

  createLineData(List<FlSpot> data){
    return [
      LineChartBarData(
      spots: data,
        isCurved: true,
        colors: gradientColors,
        barWidth: 4,
        isStrokeCapRound: false,
        dotData: FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: false,
          colors: gradientColors.map((color) => color.withOpacity(0.3)).toList(), //TODO Set a vertical gradient here
        ),
      )
    ];
  }

  String writeBottomTitle(double value){
    //TODO Rework bottom axis
    int first = 0;
    int last = checkInData.indexOf(checkInData.last);
    int middle = (last/2).round();
    if(value.round() == first || value.round() == middle || value.round() == last){
      return checkInData[value.round()].checkInDate.day.toString() + "/" + checkInData[value.round()].checkInDate.month.toString() + "/" + checkInData[value.round()].checkInDate.year.toString().substring(2);
    } else {
      return '';
    }
  }

  String writeLeftTitle(double value){
    switch(chartType){
      case ProgressLineChartData.balance:
        value = value.round().toDouble();
        if(value % 10 == 0 || value == getMaxBalance() || value == getMinBalance()){
          return value.toInt().toString() + "%";
        } else
          return "";
        break;
      case ProgressLineChartData.happiness:
        if(value == 5.0){
          return "Very\nHappy";
        } else {
          return "";
        }
        break;
    }
    return value.toString();
  }

//  METHODS RELATING TO DATA TRANSFORMATION

  int getMaxBalance(){
    int maxBalance = 0;
    checkInData.forEach((element) {
      if(element.balancePercent > maxBalance){
        maxBalance = element.balancePercent;
      }
    });
    return maxBalance;
  }

  int getMinBalance(){
    int minBalance = 100;
    checkInData.forEach((element) {
      if(element.balancePercent < minBalance){
        minBalance = element.balancePercent;
      }
    });
    return minBalance;
  }

  double getMaxY(){
    if(chartType == ProgressLineChartData.balance){
      return min(getMaxBalance().toDouble() + getMaxBalance() * 0.01, 102);
    } else if (chartType == ProgressLineChartData.happiness){
      return 5.2;
    } else return 100;
  }

  double getMinY(){
    if(chartType == ProgressLineChartData.balance){
      return max(getMinBalance().toDouble() - getMinBalance() * 0.01, 0);
    } else if (chartType == ProgressLineChartData.happiness){
      return 0;
    } else return 0;
  }

  List<FlSpot> createDataPoints(){
    List<FlSpot> points = [];
    if(chartType == ProgressLineChartData.balance){
      checkInData.forEach((element) {
        points.add(FlSpot(
            checkInData.indexOf(element).toDouble(),
            element.balancePercent.toDouble()
        ));
      });
    } else if (chartType == ProgressLineChartData.happiness){
      checkInData.forEach((element) {
        if(element.happiness != null){
          points.add(FlSpot(
            checkInData.indexOf(element).toDouble(),
            element.happiness.toDouble(),
          ));
        }
      });
    }
    return points;
  }
}