import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class ConfirmationDialog extends StatelessWidget {
  final Function yesAction;
  final String actionDesc;
  final bool reversible;
  final bool expanded;

  ConfirmationDialog({
    @required this.actionDesc,
    @required this.yesAction,
    @required this.reversible,
    this.expanded=false
});

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kAlertBorderRadius),
      ),
      title: Text("Are you sure you want to $actionDesc?", style: TextStyle(color: Theme.of(context).accentColor, fontSize: kMainTextSize),),
      content: Container(
          width: expanded ? width * 0.9 : null,
          height: expanded ? height * 0.15 : null,
          child: reversible ? SizedBox() : Text("This action cannot be undone.", style: TextStyle(color: Theme.of(context).accentColor, fontSize: kSmallTextSize),)),
      actions: <Widget>[
        FlatButton(
          child: Text(
            "Yes",
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: kMainTextSize,
            ),),
          onPressed: yesAction,
        ),
        FlatButton(
          child: Text(
            "No",
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: kMainTextSize,
            ),
          ),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ],
    );
  }
}
