import 'package:flutter/material.dart';
import 'dart:math';

class SizedIcon extends StatelessWidget {

  final IconData icon;
  final double parentChildRatio;
  final Color colour;

  SizedIcon({@required this.icon, @required this.parentChildRatio, @required this.colour});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints){
        double smallestDimension = min(constraints.maxWidth, constraints.maxHeight);
        return Icon(
          icon,
          size: smallestDimension * parentChildRatio,
          color: colour,
        );
      },
    );
  }
}
