import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/widgets/lists/project_list.dart';
import 'package:flutter/material.dart';

class ProjectDialog extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Dialog(
      insetPadding: MediaQuery.of(context).viewInsets,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
      ),
      child: SingleChildScrollView(
        child: Container(
          width: width * 0.9,
          height: height * 0.6,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.05, vertical:  height * 0.02),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                AutoSizeText(
                    "Assign a Project",
                  style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
                ),
                SizedBox(height: height * 0.025,),
                Container(
                    height: height * 0.45,
                    width: width * 0.9,
                    child: ProjectListWidget()
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}