import 'dart:collection';
import 'package:balance_app/utilities/checkIn.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/time_handler.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum ChartData{
  balance,
  happiness,
  numberWorkTasks,
  numberBalanceTasks
}

class BalanceBarChart extends StatefulWidget {

  final List<CheckIn> checkInData;
  final ChartData chartData;

  BalanceBarChart({
    @required this.checkInData,
    @required this.chartData,
});

  @override
  State<StatefulWidget> createState() => BalanceBarChartState(checkInData, chartData);
}

class BalanceBarChartState extends State<BalanceBarChart> {
  final List<CheckIn> checkInData;
  final ChartData chartData;
  Color barBackgroundColor;
  int touchedIndex;

  int maxBalance;

  List<int> months = [];
  List<String> weeks = [];

  BalanceBarChartState(this.checkInData, this.chartData);

  bool yearView = true;

  @override
  Widget build(BuildContext context) {
    barBackgroundColor = Theme.of(context).brightness == Brightness.light ? Colors.grey[100] : Colors.grey[700];

    DateTime lastDate = DateTime.now();
    DateTime firstDate = lastDate.subtract(Duration(days: yearView ? 335 : 56));

    String startMonthString = getMonth(month: firstDate.month, longString: true);
    String lastMonthString = getMonth(month: lastDate.month, longString: true);

    String startYearString = firstDate.year.toString().substring(2);
    String lastYearString = lastDate.year.toString().substring(2);

    String yearString = startMonthString + " '" + startYearString + " - " + lastMonthString + " '" + lastYearString;

    return AspectRatio(
      aspectRatio: 1,
      child: Card(
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: Theme.of(context).cardColor,
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    createTitleText(),
                    style: TextStyle(
                        color: Theme.of(context).accentColor, fontSize: kTitleTextSize),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    yearString,
                    style: TextStyle(
                        color: Theme.of(context).accentColor, fontSize: kSmallTextSize),
                  ),
                  const SizedBox(
                    height: 38,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: BarChart(
                        yearView ? yearBarData() : monthBarData(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topRight,
                child: IconButton(
                  icon: Icon(
                    FontAwesomeIcons.calendarAlt,
                    color: Theme.of(context).accentColor,
                  ),
                  onPressed: () {
                    setState(() {
                      yearView = !yearView;
                    });
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  BarChartGroupData makeGroupData(
      int x,
      double y, {
        bool isTouched = false,
        Color barColor = kLightOrange,
        double width = 16,
        List<int> showTooltips = const [],
      }) {
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          y: isTouched ? y + (createMaxY() / 10) : y,
          color: isTouched ? Theme.of(context).primaryColor : barColor,
          width: width,
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            y: createMaxY().toDouble(),
            color: barBackgroundColor,
          ),
        ),
      ],
      showingTooltipIndicators: showTooltips,
    );
  }

  BarChartData yearBarData() {
    return BarChartData(
      barTouchData: BarTouchData(
        touchTooltipData: BarTouchTooltipData(
            tooltipBgColor: Theme.of(context).primaryColor,
            getTooltipItem: (group, groupIndex, rod, rodIndex) {
              String month;
              switch (months[group.x.toInt()-1]) {
                case 0:
                  month = 'January';
                  break;
                case 1:
                  month = 'February';
                  break;
                case 2:
                  month = 'March';
                  break;
                case 3:
                  month = 'April';
                  break;
                case 4:
                  month = 'May';
                  break;
                case 5:
                  month = 'June';
                  break;
                case 6:
                  month = 'July';
                  break;
                case 7:
                  month = 'August';
                  break;
                case 8:
                  month = 'September';
                  break;
                case 9:
                  month = 'October';
                  break;
                case 10:
                  month = 'November';
                  break;
                case 11:
                  month = 'December';
                  break;
              }
              return BarTooltipItem(
                  createToolTip(month, rod.y),
                  TextStyle(color: Colors.white, fontSize: kSmallerTextSize));
            }),
        touchCallback: (barTouchResponse) {
          setState(() {
            if (barTouchResponse.spot != null &&
                barTouchResponse.touchInput is! FlPanEnd &&
                barTouchResponse.touchInput is! FlLongPressEnd) {
              touchedIndex = barTouchResponse.spot.touchedBarGroupIndex;
            } else {
              touchedIndex = -1;
            }
          });
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          textStyle: TextStyle(color: Theme.of(context).accentColor, fontSize: 14),
          margin: 16,
          getTitles: (double value) {
            print(months);
            return getMonth(month: months[value.toInt()]);
          },
        ),
        leftTitles: SideTitles(
          showTitles: false,
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: createBarDataYear(),
    );
  }

  BarChartData monthBarData() {
    return BarChartData(
      barTouchData: BarTouchData(
        touchTooltipData: BarTouchTooltipData(
            tooltipBgColor: Theme.of(context).primaryColor,
            getTooltipItem: (group, groupIndex, rod, rodIndex) {
              String descriptor;
              descriptor = createWeekDescriptor(weeks[group.x]);
              //reference the list based on x and return a describing the day and month
              return BarTooltipItem(
                  createToolTip(descriptor, rod.y),
                  TextStyle(color: Colors.white, fontSize: kSmallerTextSize));
            }),
        touchCallback: (barTouchResponse) {
          setState(() {
            if (barTouchResponse.spot != null &&
                barTouchResponse.touchInput is! FlPanEnd &&
                barTouchResponse.touchInput is! FlLongPressEnd) {
              touchedIndex = barTouchResponse.spot.touchedBarGroupIndex;
            } else {
              touchedIndex = -1;
            }
          });
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          textStyle: TextStyle(color: Theme.of(context).accentColor, fontSize: 14),
          margin: 16,
          getTitles: (double value) {
            try{
              int month = int.parse(weeks[value.toInt()].split("-")[1]);
              return getMonth(month: month, longString: true);
            } catch (e){
              return "";
            }
          },
        ),
        leftTitles: SideTitles(
          showTitles: false,
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: createBarDataMonth(),
    );
  }

  List<BarChartGroupData> createBarDataYear(){
    HashMap<String,List<CheckIn>> monthData = checkInsByMonth();
    List<BarChartGroupData> data = [];

    List<String> monthYears = [];
    months = [];

    DateTime dateToLookAt = DateTime.now();
    for(int i = 12; i > 0; i--){
      int month = dateToLookAt.month;
      int year = dateToLookAt.year;
      months.add(month);
      String monthYear = month.toString() + "-" + year.toString();
      monthYears.add(monthYear);
      if(DateTime.now().day == 31){
        dateToLookAt = dateToLookAt.subtract(Duration(days: 31));
      } else {
        dateToLookAt = dateToLookAt.subtract(Duration(days: 30));
      }

    }

    months = List.from(months.reversed);

    for(int i = 0; i < 12; i++){
      int xPos = 11 - i;
      BarChartGroupData bar;
      if(monthData.containsKey(monthYears[i])){
        int dataTotal = 0;
        monthData[monthYears[i]].forEach((element) {
          switch(chartData){
            case ChartData.balance:
              dataTotal += element.balancePercent;
              double dataAverage = (dataTotal / monthData[monthYears[i]].length);
              bar = makeGroupData(xPos, dataAverage, isTouched: xPos == touchedIndex);
              break;
            case ChartData.happiness:
              dataTotal += element.happiness;
              double dataAverage = (dataTotal / monthData[monthYears[i]].length);
              bar = makeGroupData(xPos, dataAverage, isTouched: xPos == touchedIndex);
              break;
            case ChartData.numberWorkTasks:
              dataTotal += element.workTasksCompleted;
              bar = makeGroupData(xPos, dataTotal.toDouble(), isTouched: xPos == touchedIndex);
              break;
            case ChartData.numberBalanceTasks:
              dataTotal += element.balanceTasksCompleted;
              bar = makeGroupData(xPos, dataTotal.toDouble(), isTouched: xPos == touchedIndex);
              break;
          }
        });


      } else {
        bar = makeGroupData(xPos, 0, isTouched: xPos == touchedIndex);
      }
      data.add(bar);
    }
    return List.from(data.reversed);
  }

  List<BarChartGroupData> createBarDataMonth(){
    HashMap<String,List<CheckIn>> weekData = checkInsByWeek();
    List<BarChartGroupData> data = [];

    List<String> weekMonths = [];

    DateTime dateToLookAt = DateTime.now();
    for(int i = 8; i > 0; i--){
      int month = dateToLookAt.month;
      int week = (dateToLookAt.day / 8).ceil();
      String weekMonth = week.toString() + "-" + month.toString();
      weeks.add(weekMonth);
      weekMonths.add(weekMonth);
      dateToLookAt = dateToLookAt.subtract(Duration(days: 8));
    }

    weeks = List.from(weeks.reversed);

    for(int i = 0; i < 8; i++){
      int xPos = 7 - i;
      BarChartGroupData bar;
      if(weekData.containsKey(weekMonths[i])){
        int dataTotal = 0;
        weekData[weekMonths[i]].forEach((element) {
          switch(chartData){
            case ChartData.balance:
              dataTotal += element.balancePercent;
              double dataAverage = (dataTotal / weekData[weekMonths[i]].length);
              bar = makeGroupData(xPos, dataAverage, isTouched: xPos == touchedIndex);
              break;
            case ChartData.happiness:
              dataTotal += element.happiness;
              double dataAverage = (dataTotal / weekData[weekMonths[i]].length);
              bar = makeGroupData(xPos, dataAverage, isTouched: xPos == touchedIndex);
              break;
            case ChartData.numberWorkTasks:
              dataTotal += element.workTasksCompleted;
              bar = makeGroupData(xPos, dataTotal.toDouble(), isTouched: xPos == touchedIndex);
              break;
            case ChartData.numberBalanceTasks:
              dataTotal += element.balanceTasksCompleted;
              bar = makeGroupData(xPos, dataTotal.toDouble(), isTouched: xPos == touchedIndex);
              break;
          }
        });
      } else {
        bar = makeGroupData(xPos, 0, isTouched: xPos == touchedIndex);
      }
      data.add(bar);
    }

    return List.from(data.reversed);
  }

  HashMap<String,List<CheckIn>> checkInsByMonth(){
    HashMap<String,List<CheckIn>> monthData = HashMap();

    checkInData.sort((a,b) => a.checkInDate.compareTo(b.checkInDate));
    checkInData.forEach((element) {

      String monthYear = element.checkInDate.month.toString() + "-" + element.checkInDate.year.toString();
      if(monthData.containsKey(monthYear)){
        monthData[monthYear].add(element);
      } else {
        List<CheckIn> mapElement = [element];
        monthData.putIfAbsent(monthYear, () => mapElement);
      }
    });

    return monthData;
  }

  HashMap<String,List<CheckIn>> checkInsByWeek(){
    HashMap<String,List<CheckIn>> weekData = HashMap();

    checkInData.sort((a,b) => a.checkInDate.compareTo(b.checkInDate));
    checkInData.forEach((element) {
      int week = (element.checkInDate.day / 8).ceil();
      String weekMonth = week.toString() + "-" + element.checkInDate.month.toString();
      if(weekData.containsKey(weekMonth)){
        weekData[weekMonth].add(element);
      } else {
        List<CheckIn> mapElement = [element];
        weekData.putIfAbsent(weekMonth, () => mapElement);
      }
    });

    return weekData;
  }

  String getMonth({int month, bool longString=false}){
    switch (month) {
      case 1:
        if(longString) return "Jan"; else return 'J';
        break;
      case 2:
        if(longString) return "Feb"; else return 'F';
        break;
      case 3:
        if(longString) return "Mar"; else return 'M';
        break;
      case 4:
        if(longString) return "Apr"; else return 'A';
        break;
      case 5:
        if(longString) return "May"; else return 'M';
        break;
      case 6:
        if(longString) return "Jun"; else return 'J';
        break;
      case 7:
        if(longString) return "Jul"; else return 'J';
        break;
      case 8:
        if(longString) return "Aug"; else return 'A';
        break;
      case 9:
        if(longString) return "Sep"; else return 'S';
        break;
      case 10:
        if(longString) return "Oct"; else return 'O';
        break;
      case 11:
        if(longString) return "Nov"; else return 'N';
        break;
      case 12:
        if(longString) return "Dec"; else return 'D';
        break;
      default:
        return '';
    }
  }

  String createWeekDescriptor(String date){
    List<String> components = date.split("-");
    int week = int.parse(components[0]);
    int month = int.parse(components[1]);

    int daysInMonth = lastDayOfMonth(month).day;

    switch(week){
      case 1:
        return getMonth(month: month, longString: true) + " 1" + " - 8";
        break;
      case 2:
        return getMonth(month: month, longString: true) + " 9" + " - 16";
        break;
      case 3:
        return getMonth(month: month, longString: true) + " 17" + " - 24";
        break;
      case 4:
        return getMonth(month: month, longString: true) + " 25" + " - $daysInMonth";
        break;
      default:
        return "";
        break;
    }
  }

  String happinessString(int y){
    switch(y){
      case 1:
        return "Very Sad";
        break;
      case 2:
        return "Sad";
        break;
      case 3:
        return "Okay";
        break;
      case 4:
        return "Happy";
        break;
      case 5:
        return "Very Happy";
        break;
    }
    return "";
  }

  String createTitleText(){
    switch(chartData){
      case ChartData.balance:
        return "Your Balance";
        break;
      case ChartData.happiness:
        return "Your Happiness";
        break;
      case ChartData.numberBalanceTasks:
        return "Amount of Rest";
        break;
      case ChartData.numberWorkTasks:
        return "Amount of Work";
        break;
      default:
        return "";
    }
  }

  int createMaxY(){
    switch(chartData){
      case ChartData.balance:
        return 100;
        break;
      case ChartData.happiness:
        return 5;
        break;
      case ChartData.numberBalanceTasks:
      case ChartData.numberWorkTasks:
        return getMaxTasks() == 0 ? 100 : getMaxTasks(); //todo create a function for calculating this
        break;
      default:
        return 100;
    }
  }

  String createToolTip(String descriptor, double yValue){
    switch(chartData){
      case ChartData.balance:
        return descriptor + "\n" + (yValue.round() - createMaxY()/10).round().toString() + "%";
        break;
      case ChartData.happiness:
        return descriptor + "\n" + happinessString(yValue.round() - (createMaxY()/10).round());
        break;
      case ChartData.numberBalanceTasks:
      case ChartData.numberWorkTasks:
        String returnString;
        yValue.round() == 1 ?
        returnString = descriptor + "\n" + (yValue.round() - createMaxY()/10).round().toString() + " Task" :
        returnString = descriptor + "\n" + (yValue.round() - createMaxY()/10).round().toString() + " Tasks";
        return returnString;
        break;
      default:
        return "";
    }
  }

  int getMaxTasks(){
    Map<String,List<CheckIn>> separatedCheckIns;
    if(yearView){
      separatedCheckIns = checkInsByMonth();
    } else {
      separatedCheckIns = checkInsByWeek();
    }

    int maxTasks = 0;
    if(separatedCheckIns.isEmpty){
      return 100;
    }
    separatedCheckIns.values.forEach((element) {
      int currMaxTasks = 0;
      element.forEach((element) {
        switch(chartData){
          case ChartData.numberWorkTasks:
            currMaxTasks += element.workTasksCompleted;
            break;
          case ChartData.numberBalanceTasks:
            currMaxTasks += element.balanceTasksCompleted;
            break;
          default:
            break;
        }
      });
      if(currMaxTasks > maxTasks) maxTasks = currMaxTasks;
    });

    return maxTasks;
  }

}