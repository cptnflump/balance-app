import 'dart:ui';

import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class BalanceTutorial extends StatelessWidget {
  final Function onTap;

  BalanceTutorial({
    @required this.onTap,
});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: Card(
            elevation: 4.0,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Theme.of(context).primaryColor, width: 1.5),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: width * 0.025, vertical: height * 0.02),
              child: Column(
                children: [
                  Text(
                    "The Balance Bar",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kMainTextSize,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.01,
                  ),
                  Text(
                    "This shows you your current work/rest balance, as well as your current goal balance.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.01,
                  ),
                  Text(
                    "Also included is the balance bar, a red bar means an imbalance heavily towards work, while a green bar shows an imbalance heavily towards rest, aim for orange!",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.01,
                  ),
                  GestureDetector(
                    onTap: onTap,
                    child: Text(
                      "Okay",
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: kMainTextSize
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.01,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}