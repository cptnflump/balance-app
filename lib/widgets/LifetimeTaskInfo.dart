import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:flutter/material.dart';

class LifetimeTaskInfo extends StatelessWidget {
  const LifetimeTaskInfo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Row(
          children: [
            AutoSizeText(
              "Balance Based Tasks: ",
              maxLines: 1,
              style: TextStyle(
                fontSize: kSmallTextSize,
                color: Theme.of(context).accentColor,
              ),
            ),
            AutoSizeText(
              userLifetimeCompletedBalance.toString(),
              maxLines: 1,
              style: TextStyle(
                fontSize: kSmallTextSize,
                color: Theme.of(context).accentColor,
              ),
            ),
          ],
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
        Row(
          children: [
            AutoSizeText(
              "Work Based Tasks: ",
              maxLines: 1,
              style: TextStyle(
                fontSize: kSmallTextSize,
                color: Theme.of(context).accentColor,
              ),
            ),
            AutoSizeText(
              userLifetimeCompletedTasks.toString(),
              maxLines: 1,
              style: TextStyle(
                fontSize: kSmallTextSize,
                color: Theme.of(context).accentColor,
              ),
            ),
          ],
        )
      ],
    );
  }
}