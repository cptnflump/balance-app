import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class BalanceBar extends StatefulWidget {
  const BalanceBar({
    @required this.barColour,
    @required this.balance,
  });

  final Color barColour;
  final int balance;

  @override
  _BalanceBarState createState() => _BalanceBarState();
}

class _BalanceBarState extends State<BalanceBar> with SingleTickerProviderStateMixin {

  AnimationController _controller;
  Tween<double> valueTween;
  double oldValue;



  @override
  void initState() {
    oldValue = widget.balance/100;
    super.initState();
    this._controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );
    this._controller.forward();
  }

  @override
  Widget build(BuildContext context) {

    valueTween = Tween<double>(
      begin: oldValue,
      end: widget.balance/100,
    );

    oldValue = widget.balance/100;

    return AnimatedBuilder(
      animation: this._controller,
      child: Container(),
      builder: (context,child){
        return LinearProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(widget.barColour),
          backgroundColor: Theme.of(context).brightness == Brightness.light ? kGreyWhite : kGrey,
          value: valueTween.evaluate(_controller),
//          value: widget.balance/100,
        );
      },
    );
  }

  @override
  void didUpdateWidget(BalanceBar oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (this.widget.balance != oldWidget.balance) {
      // Try to start with the previous tween's end value. This ensures that we
      // have a smooth transition from where the previous animation reached.
      double beginValue =
          this.valueTween?.evaluate(this._controller) ?? oldWidget.balance/100 ?? 0;

      // Update the value tween.
      this.valueTween = Tween<double>(
        begin: beginValue,
        end: this.widget.balance/100 ?? 1,
      );

      this._controller
        ..value = 0
        ..forward();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
