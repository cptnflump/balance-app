#!/bin/bash
#This script build a new android APK and uploads it to firebase for distribution

distribute(){

  echo "Starting iOS build"

  cd ios

  xcodebuild -workspace Runner.xcworkspace \
            -scheme Runner \
            -destination generic/platform=iOS build

  echo "Starting Archiving"

  xcodebuild -workspace Runner.xcworkspace -scheme Runner -sdk iphoneos -configuration AppStoreDistribution archive -archivePath $PWD/build/Runner.xcarchive

  echo "Creating ipa file"

  xcodebuild -exportArchive -allowProvisioningUpdates -archivePath $PWD/build/Runner.xcarchive -exportOptionsPlist exportOptions.plist -exportPath $PWD/build

  echo "ipa file created"

  cd ..

  mv ios/build/Blossum.ipa Blossum.ipa

  firebase appdistribution:distribute Blossum.ipa --app 1:700364450072:ios:8be163258829d14aa0166d --release-notes-file "ios_distribution/ios_release_notes.txt" --testers-file "ios_distribution/ios_testers.txt"

  rm ios_release.ipa

  echo "Distribtution to firebase complete"
}

while true; do
    read -p "Have you updated the release notes and build number?" yn
    case $yn in
        [Yy]* ) distribute; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done



