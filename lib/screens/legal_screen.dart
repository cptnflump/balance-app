import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/widgets/profile_row.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class LegalScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Legal",
          style: TextStyle(
              color: Colors.white,
              fontSize: kTitleTextSize
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(top: height * 0.01),
          child: Column(
            children: [
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: null,
                rowText: "End User License Agreement",
                onTap: (){
                  _launchURL("https://www.youfluent.co.uk/blossum-eula");
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Divider(
                  color: Theme.of(context).primaryColor,
                  thickness: 1,
                ),
              ),
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: null,
                rowText: "Privacy Policy",
                onTap: (){
                  _launchURL("https://www.youfluent.co.uk/privacy");
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}
