import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/task.dart';
import 'package:balance_app/utilities/time_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/blur_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../alerts/task_alerts/task_options_modal.dart';


class TaskListWidget extends StatefulWidget {
  @override
  _TaskListWidgetState createState() => _TaskListWidgetState();
}

class _TaskListWidgetState extends State<TaskListWidget> {


  double height;
  double width;
  List<Task> tasks;

  @override
  Widget build(BuildContext context) {

    if(listFilter == null) filteredUserTasks = userTasks;

    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    tasks = filteredUserTasks;

    return Padding(
      padding: EdgeInsets.only(left: width * 0.01, right: width * 0.01),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15)
        ),
        child: Padding(
          padding: EdgeInsets.all(height * 0.015),
          child: ListView.builder(
              itemCount: tasks.length,
              itemBuilder: _getListItem,
          ),
        ),
      ),
    );
  }

  void showTaskOptionModal(Task task) async {
    await showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: const Radius.circular(10), topRight: const Radius.circular(10)),
        ),
        context: context,
        builder: (context) => TaskOptionsModal(task: task,)
    );
    setState(() {});
  }

  Widget _getListItem(BuildContext context, int index){
    String dueDate = parseDate(tasks[index].taskDue);

    return Column(
      children: [
        Card(
          elevation: 4.0,
          shape:  RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onLongPress: (){
                setState(() {showTaskOptionModal(tasks[index]);});
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.03, vertical: height * 0.015),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        constraints: BoxConstraints(minWidth: width * 0.7, maxWidth:  width * 0.7 ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: height * 0.01),
                          child: AutoSizeText(
                            tasks[index].taskTitle,
                            style: TextStyle(
                              fontSize: kSmallTextSize,
                              color: Theme.of(context).accentColor,
                              fontFamily: 'Nunito',
                              decoration: tasks[index].isComplete ? TextDecoration.lineThrough : null,
                            ),
                            maxLines: 3,
                          ),
                        ),
                      ),

                      Transform.scale(
                        scale: 1.2,
                        child: Checkbox(
                          value: tasks[index].isComplete,
                          activeColor: Theme.of(context).primaryColor,
                          onChanged: (bool value) {
                            setState(() {
                              value = !value;
                              tasks[index].isComplete = !tasks[index].isComplete;
                              if(tasks[index].isComplete){
                                userCompletedTasks.add(tasks[index]);
                              } else {
                                if(userCompletedTasks.contains(tasks[index])){
                                  userCompletedTasks.remove(tasks[index]);
                                }
                              }
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                  tasks[index].project != null ? AutoSizeText(
                    tasks[index].project,
                    style: TextStyle(
                fontSize: kSmallestTextSize,
                color: Theme.of(context).accentColor,
              ),
                  ) : SizedBox(),
                  SizedBox(height: tasks[index].project != null ? height * 0.01 : 0,),
                  Row(
                    children: <Widget>[
                      Container(
                        constraints: BoxConstraints(minWidth: width * 0.30, maxWidth:  width * 0.7 ),
                        child: Text(
                          "$dueDate",
                          style: TextStyle(
                fontSize: kSmallestTextSize,
                color: Theme.of(context).accentColor,
              ),
                        ),
                      ),
                      tasks[index].isPriority ? Icon(
                        FontAwesomeIcons.burn,
                        size: height * 0.025,
                        color: tasks[index].isPriority ? Theme.of(context).primaryColor : Theme.of(context).accentColor,
                      ) : SizedBox(),
                      SizedBox(width: tasks[index].isPriority ? width * 0.03 : 0,),
                      tasks[index].isBalance ? Icon(
                        FontAwesomeIcons.spa,
                        size: height * 0.025,
                        color: tasks[index].isBalance ? Theme.of(context).primaryColor : Theme.of(context).accentColor,
                      ) : SizedBox(),
                    ],
                  ),
                  SizedBox(height: height * 0.01,)
                ],
              ),
            ),
          ),
        ),
        SizedBox(height: height * 0.01,),
      ],
    );
  }

  List<Widget> createChildren(){
    List<Widget> widgets = [];
    for(Task listData in tasks){
      widgets.add(
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: AutoSizeText(
              listData.taskTitle,
              style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
              maxLines: 1,
            ),
          )
      );
      widgets.add(
          Divider(
            thickness: 1.0,
            color: Theme.of(context).primaryColor,
          )
      );
    }
    widgets.removeLast();
    return widgets;
  }

  String parseDate(DateTime dateTime) {

    if(dateTime == null){
      return "No due date";
    }

    DateTime today = startOfDay(DateTime.now());
    int days = dateTime
        .difference(today)
        .inDays;
    if(days == -1){
      return "Due yesterday";
    } else if(days < 0){
      days = -days;
      return "Due $days days ago";
    } else if (days == 0) {
      return "Due today";
    } else if(days == 1) {
      return "Due tomorrow";
    } else {
      return "Due in $days days";
    }
  }



}

