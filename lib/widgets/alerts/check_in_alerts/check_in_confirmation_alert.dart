import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:flutter/material.dart';

enum BalanceDirection{
  balance,
  work,
  none,
}

class CheckInConfirmationAlert extends StatefulWidget {
  final int happiness;

  CheckInConfirmationAlert({
    @required this.happiness,
  });

  @override
  _CheckInConfirmationAlertState createState() => _CheckInConfirmationAlertState(happiness);
}

class _CheckInConfirmationAlertState extends State<CheckInConfirmationAlert> {

  int happiness;
  _CheckInConfirmationAlertState(this.happiness);

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;


    String bodyText;
    switch(widget.happiness){
      case 5:
        bodyText = "Great! \n\nI'll set your new goal as the balance you've currently achieved.";
        break;
      case 4:
        bodyText = "Great! \n\nNext I need you to tell me whether you want more work, more rest or if you like the balance you currently have.";
        break;
      case 3:
        bodyText = "You're getting there! \n\nNext I need you to tell me whether you want more work, more rest or if you like the balance you currently have.";
        break;
      case 2:
        bodyText = "I'm sorry to hear. \n\nCan you tell me whether you'd like more work or more rest and I'll shift your balance goal. \n\nAlternatively you can aim for the balance you're currently achieving.";
        break;
      case 1:
        bodyText = "I'm sorry to hear. \n\nCan you tell me whether you'd like more work or more rest and I'll significantly shift your balance goal. \n\nAlternatively you can aim for the balance you're currently achieving.";
        break;
    }

    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kAlertBorderRadius),
      ),
        title: Text(
          "Moving Forward",
          style: TextStyle(
              color: Theme.of(context).accentColor,
              fontSize: kMainTextSize
          ),
        ),
      content: Container(
        width: width * 0.9,
        height: widget.happiness < 5 ? null : height * 0.2,
        child: SingleChildScrollView(
          child: Column(
            children: [
              AutoSizeText(
                bodyText,
                style: TextStyle(color: Theme.of(context).accentColor, fontSize: kSmallerTextSize),
              ),
              widget.happiness < 5 ? Column(
                children: [
                  SizedBox(height: height * 0.05,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () {
                          changeUserGoal(BalanceDirection.work);
                          update();
                          Navigator.pushReplacementNamed(context, 'balance');
                        },
                        child: AutoSizeText(
                          "More Work",
                          maxLines: 1,
                          style: TextStyle(
                            color: Theme.of(context).primaryColor
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          changeUserGoal(BalanceDirection.balance);
                          update();
                          Navigator.pushReplacementNamed(context, 'balance');
                        },
                        child: AutoSizeText(
                          "More Rest",
                          maxLines: 1,
                          style: TextStyle(
                            color: Theme.of(context).primaryColor
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: height * 0.03,),
                  Center(
                    child: GestureDetector(
                      onTap: () {
                        update();
                        Navigator.pushReplacementNamed(context, 'balance');
                      },
                      child: AutoSizeText(
                        "Keep Current Goal",
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Theme.of(context).primaryColor
                        ),
                      ),
                    ),
                  ),
                ],
              ) : SizedBox(),
            ],
          ),
        ),
      ),
      actions: [
        widget.happiness > 4 ? Padding(
          padding: EdgeInsets.only(bottom: height * 0.02, right: width * 0.03),
          child: GestureDetector(
            onTap: () {
              if(widget.happiness != 5){
                changeUserGoal(BalanceDirection.none);
              } else {
                userGoal = userBalance;
              }
              update();
              Navigator.pushReplacementNamed(context, 'balance');
            },
            child: Text(
              "Let's go!",
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: kSmallTextSize
              ),
            ),
          ),
        ) : SizedBox(),
      ],
    );
  }

  update() async {
    await updateCheckInList(happiness);
    await updateGoal();
    userNumberCompletedBalance = 0;
    userNumberCompletedTasks = 0;
    userBalance = 50;
    userLastCheckIn = DateTime.now();
  }

  changeUserGoal(BalanceDirection direction){
    //    Here should be the logic for updating the users new balance based on happiness and the direction they want to go in
    double multiplier;
    switch(widget.happiness){
      case 4:
        multiplier = 1.1;
        break;
      case 3:
        multiplier = 1.2;
        break;
      case 2:
        multiplier = 1.3;
        break;
      case 1:
        multiplier = 1.2;
        break;
    }

    if(direction == BalanceDirection.none){
      if(userBalance < userGoal){
        direction = BalanceDirection.work;
      } else {
        direction = BalanceDirection.balance;
      }
    }

    if(direction == BalanceDirection.balance){
      userGoal = userBalance * multiplier;
    } else {
      multiplier = 1 - (multiplier - 1);
      userGoal = userBalance * multiplier;
    }

    if(userGoal > 100){
      userGoal = 100;
    } else if(userGoal < 0){
      userGoal = 0;
    }

  }

}
