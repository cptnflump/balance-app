import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/image_widgets/loading_circle.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

enum Provider{
  apple,
  google,
  password
}

class DeleteAccountScreen extends StatefulWidget {

  DeleteAccountScreen();

  @override
  _DeleteAccountScreenState createState() => _DeleteAccountScreenState();
}

class _DeleteAccountScreenState extends State<DeleteAccountScreen> {

  final _formKey = GlobalKey<FormState>();
  String errorMessage;
  Future<Provider> currProvider;


  TextEditingController passwordController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
    currProvider = checkCurrProvider();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return FutureBuilder(
        future: currProvider,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Container(); //todo here add an error message
            } else {
              print(snapshot.data);
              return Scaffold(
                appBar: AppBar(
                  backgroundColor: Theme.of(context).primaryColor,
                  iconTheme: IconThemeData(
                    color: Colors.white,
                  ),
                  title: Text(
                    "Delete Account",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: kTitleTextSize
                    ),
                  ),
                ),
                body: Form(
                  key: _formKey,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                    child: snapshot.data == Provider.password ? Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: height * 0.02,),
                        Visibility(
                          visible: errorMessage != null,
                          child: AutoSizeText(
                            errorMessage == null ? "" : errorMessage,
                            style: TextStyle(
                                color: Colors.red, fontSize: kSmallerTextSize),
                          ),
                        ),
                        SizedBox(height: height * 0.02,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              "Email Address" ,
                              style: TextStyle(
                                fontSize: kSmallerTextSize,
                                color: Theme.of(context).accentColor,
                              ),
                            ),
                            TextFormField(
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration(
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: kOrange),
                                ),
                              ),
                              style: TextStyle(fontSize: kSmallerTextSize),
                              controller: emailController,
                              obscureText: false,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Please Enter Some Text";
                                } else
                                  return null;
                              },
                            )
                          ],
                        ),
                        snapshot.data == Provider.password ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              "Password",
                              style: TextStyle(
                                fontSize: kSmallerTextSize,
                                color: Theme.of(context).accentColor,
                              ),
                            ),
                            TextFormField(
                              cursorColor: Theme.of(context).primaryColor,
                              style: TextStyle(fontSize: kSmallerTextSize),
                              decoration: InputDecoration(
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: kOrange),
                                ),
                              ),
                              controller: passwordController,
                              obscureText: true,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Please Enter Some Text";
                                } else
                                  return null;
                              },
                            )
                          ],
                        ) : SizedBox(),
                        SizedBox(height: height * 0.02,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                              splashColor: Theme.of(context).primaryColor.withOpacity(0.5),
                              child: Text(
                                "Cancel",
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: kSmallTextSize
                                ),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            FlatButton(
                              splashColor: Theme.of(context).primaryColor.withOpacity(0.5),
                              child: Text(
                                "Delete Account",
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: kSmallTextSize
                                ),
                              ),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  String email = emailController.text
                                      .trim();
                                  String pass = passwordController.text.trim();
                                    reAuthAndChangeEmail(context: context,
                                        email: email,
                                        pass: pass);
                                }
                              },
                            ),
                          ],
                        ),
                      ],
                    ) : Center(
                      child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        onPressed: (){
                          if(snapshot.data == Provider.google){
                            reAuthWithGoogle();
                          } else {
                            reAuthWithApple();
                          }
                        },
                        child: Text(
                          "Confirm Account Deletion",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: kMainTextSize,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            }
          } else {
            return Scaffold(body: SafeArea(child: Center(child: LoadingCircle(width: width,))));
          }
        });
  }

  /// This returns a string which is an error message, if the error message is null there was no error.
  reAuthAndChangeEmail({BuildContext context, String email, String pass}) async{
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    if(email != user.email){
      setState(() {
        errorMessage = "Email Does Not Match.";
      });
    } else{
      AuthCredential credential = EmailAuthProvider.getCredential(
          email: user.email,
          password: pass
      );
      await user.reauthenticateWithCredential(credential).catchError((error){
        print(error);
        errorMessage = "Password Does Not Match.";
      });

      if(errorMessage == null){
        doDelete(user);
      } else {
        setState(() {});
      }
    }
  }

  reAuthWithGoogle() async{
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
      if(errorMessage != null){
        final GoogleSignIn _googleSignIn = GoogleSignIn();
        final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
        final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        await FirebaseAuth.instance.signInWithCredential(credential);

        doDelete(user);
      }



  }

  reAuthWithApple() async {
      final AuthorizationResult result = await AppleSignIn.performRequests([
        AppleIdRequest()
      ]);
      switch (result.status) {
        case AuthorizationStatus.authorized:
          print("successful sign in");
          final AppleIdCredential appleIdCredential = result.credential;

          OAuthProvider oAuthProvider =
          new OAuthProvider(providerId: "apple.com");
          final AuthCredential credential = oAuthProvider.getCredential(
            idToken:
            String.fromCharCodes(appleIdCredential.identityToken),
            accessToken:
            String.fromCharCodes(appleIdCredential.authorizationCode),
          );

          await FirebaseAuth.instance.signInWithCredential(credential);
          FirebaseUser user = await FirebaseAuth.instance.currentUser();

          doDelete(user);

          break;
        case AuthorizationStatus.error:
          print("Error with Apple Sign in");
          break;

        case AuthorizationStatus.cancelled:
          print('User cancelled');
          break;
      }



  }


  //changing email:
  //check current platform
  //if on android check if there is a google provider and auth with google
  //if on android and no google provider auth with password
  //if on iOS check if there is an apple provider and auth with apple
  //if on iOS and no apple provider check if can auth with google
  // if on iOS and neither providers present auth with password
  Future<Provider> checkCurrProvider() async{
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    List<String> providers = [];
    print(user.providerId);
    print("");
    user.providerData.forEach((element) {
      if(!providers.contains(element.providerId)) providers.add(element.providerId);
    });
    print(providers);

    TargetPlatform platform = Theme.of(context).platform;
    if(platform == TargetPlatform.android){
      if(providers.contains("google.com")){
        return Provider.google;
      } else {
        return Provider.password;
      }
    } else {
      setState(() {});
      if(providers.contains("apple.com")){
        return Provider.apple;
      } else if(providers.contains("google.com")){
        return Provider.google;
      } else {
        return Provider.password;
      }
    }
  }

  doDelete(FirebaseUser user) {
    print("DELETING");
    if(errorMessage == null){
      try{
        deleteAccount(context);
      } catch (error) {
        print(error);
        errorMessage = "New Email is not valid.";
        setState(() {});
      }
    } else {
      setState(() {
      });
    }
  }


  @override
  void dispose() {
    emailController.dispose();
    super.dispose();
  }
}
