import 'dart:collection';
import 'dart:math';
import 'package:balance_app/utilities/balance_handler.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/time_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/widgets/alerts/check_in_alerts/check_in_alert.dart';
import 'package:balance_app/widgets/alerts/time_away_alert.dart';
import 'package:balance_app/widgets/balance_bar.dart';
import 'package:balance_app/widgets/blur_screen.dart';
import 'package:balance_app/widgets/navigation_bar.dart';
import 'package:balance_app/widgets/tutorials/balance_screen/balance_tutorial.dart';
import 'package:balance_app/widgets/tutorials/balance_screen/blossum_explanation.dart';
import 'package:balance_app/widgets/tutorials/balance_screen/checkIn_progress_tutorial.dart';
import 'package:balance_app/widgets/tutorials/balance_screen/work_rest_tutorial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rate_my_app/rate_my_app.dart';

class BalanceScreen extends StatefulWidget {
  @override
  _BalanceScreenState createState() => _BalanceScreenState();
}

class _BalanceScreenState extends State<BalanceScreen> with WidgetsBindingObserver {

  RateMyApp rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 3,
    remindDays: 3,
    remindLaunches: 5,
    minLaunches: 5, // Show rate popup after 5 launches of app after minDays is passed.
  );

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int tutorialStage = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if(userLastAppOpen != null && userHasSeenBalance && DateTime.now().difference(userLastAppOpen).inHours > 1 && userHasOpenedAppToday()){
        await showDialog(
          context: context,
          builder: (context) => TimeAwayAlert(scaffoldKey: _scaffoldKey,),
        );
        setState(() {});
        logAppOpen();
        userLastAppOpen = DateTime.now();
      } else {
        logAppOpen();
        userLastAppOpen = DateTime.now();
      }
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    rateMyApp.init();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    int balance = calculateBalance();
    Color barColour = getBalanceBarColour(context);

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _scaffoldKey,
        bottomNavigationBar: NavigationBar(
          disableBalance: true,
        ),
        appBar: AppBar(
          title: Text(
            "Balance",
            style: TextStyle(
              fontSize: kTitleTextSize,
              color: Colors.white,
            ),
          ),
          backgroundColor: Theme.of(context).primaryColor,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          leading: Container(),
        ),
        body: SafeArea(
          child: RefreshIndicator(
            color: Theme.of(context).primaryColor,
            onRefresh: (){
              updateCompleted();
              return refreshTasks();
            },
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Stack(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Stack(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                                    child: Row(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Theme.of(context).primaryColor,
                                                    width: 1.0)),
                                          ),
                                          child: AutoSizeText(
                                            createGreeting(),
                                            style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: kBigTextSize,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: width * 0.01,
                                        ),
                                        Expanded(
                                          child: Image.asset(createTimedImageAsset()),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Visibility(
                                    visible: !userHasSeenBalance,
                                    child: BlurScreen(),
                                  ),
                                ],
                              ),
                              Stack(
                                children: [
                                  Align(
                                    child: Container(
                                      width: width * 0.9,
                                      child: Card(
                                        elevation: 4.0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: height * 0.03),
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(left: width * 0.1),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      child: AutoSizeText(
                                                        "Balance:",
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                .accentColor,
                                                            fontSize: kTitleTextSize,
                                                            fontFamily: 'Nunito'),
                                                      ),
                                                      width: width * 0.5,
                                                    ),
                                                    Expanded(
                                                      child: Padding(
                                                        padding: EdgeInsets.only(
                                                            right: width * 0.1),
                                                        child: Row(
                                                          textBaseline:
                                                              TextBaseline.alphabetic,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment.end,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .baseline,
                                                          children: <Widget>[
                                                            Text(
                                                              balance.toString(),
                                                              maxLines: 1,
                                                              style: TextStyle(
                                                                  color:
                                                                      Theme.of(context)
                                                                          .accentColor,
                                                                  fontSize:
                                                                      kTitleTextSize,
                                                                  fontFamily: 'Nunito'),
                                                            ),
                                                            Text(
                                                              "%",
                                                              style: TextStyle(
                                                                  color:
                                                                      Theme.of(context)
                                                                          .accentColor,
                                                                  fontSize:
                                                                      kSmallerTextSize,
                                                                  fontFamily: 'Nunito'),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: width * 0.1,
                                                    bottom: height * 0.02),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      child: AutoSizeText(
                                                        "Goal:",
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            color: Theme.of(context)
                                                                .accentColor,
                                                            fontSize: kTitleTextSize,
                                                            fontFamily: 'Nunito'),
                                                      ),
                                                      width: width * 0.5,
                                                    ),
                                                    Expanded(
                                                      child: Padding(
                                                        padding: EdgeInsets.only(
                                                            right: width * 0.1),
                                                        child: Row(
                                                          textBaseline:
                                                              TextBaseline.alphabetic,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment.end,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .baseline,
                                                          children: <Widget>[
                                                            Text(
                                                              userGoal == null
                                                                  ? "50"
                                                                  : userGoal
                                                                      .round()
                                                                      .toString(),
                                                              maxLines: 1,
                                                              style: TextStyle(
                                                                  color:
                                                                      Theme.of(context)
                                                                          .accentColor,
                                                                  fontSize:
                                                                      kTitleTextSize,
                                                                  fontFamily: 'Nunito'),
                                                            ),
                                                            Text(
                                                              "%",
                                                              style: TextStyle(
                                                                  color:
                                                                      Theme.of(context)
                                                                          .accentColor,
                                                                  fontSize:
                                                                      kSmallerTextSize,
                                                                  fontFamily: 'Nunito'),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
//                            SizedBox(height: height * 0.02,),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: width * 0.1,
                                                    right: width * 0.1),
                                                child: Container(
                                                  height: height * 0.05,
                                                  decoration: BoxDecoration(
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Theme.of(context)
                                                                    .brightness ==
                                                                Brightness.light
                                                            ? Theme.of(context)
                                                                .accentColor
                                                                .withOpacity(0.6)
                                                            : Colors.black,
                                                        offset: new Offset(0, 4.0),
                                                        blurRadius: 10.0,
                                                        spreadRadius: -5.0,
                                                      )
                                                    ],
                                                  ),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(10),
                                                    child: BalanceBar(
                                                        barColour: barColour,
                                                        balance: balance),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Visibility(
                            //here we will show the tutorial for stage 2 and 3
                            visible: !userHasSeenBalance && tutorialStage != 1,
                            child: BlurScreen(
                              child: getTutorial(1),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: height * 0.025,),
                      Stack(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Stack(
                                children: [
                                  Column(
                                    children: [
                                      Align(
                                        child: Container(
                                          width: width * 0.9,
                                          height: height * 0.15,
                                          child: Card(
                                            elevation: 4.0,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(20),
                                            ),
                                            child: Padding(
                                              padding:
                                                  EdgeInsets.symmetric(vertical: height * 0.03),
                                              child: LayoutBuilder(
                                                builder: (context, constraints) => Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.spaceEvenly,
                                                  children: [
                                                    Expanded(
                                                      child: InkWell(
                                                        splashColor:
                                                            Theme.of(context).primaryColor,
                                                        customBorder: CircleBorder(),
                                                        onTap: () async {
                                                          await incrementTaskNumber(
                                                              isBalance: false);
                                                          showConfirmationSnackBar(
                                                              height: height,
                                                              text: "Work Added");
                                                          setState(() {});
                                                        },
                                                        child: Column(
                                                          mainAxisSize: MainAxisSize.min,
                                                          children: [
                                                            Icon(
                                                              FontAwesomeIcons.arrowDown,
                                                              color: Theme.of(context)
                                                                  .primaryColor,
                                                              size: min(
                                                                      constraints
                                                                          .constrainHeight(),
                                                                      constraints
                                                                          .constrainWidth()) *
                                                                  0.4,
                                                            ),
                                                            SizedBox(
                                                              height: constraints
                                                                      .constrainHeight() *
                                                                  0.1,
                                                            ),
                                                            AutoSizeText(
                                                              "Work",
                                                              style: TextStyle(
                                                                fontSize: kMainTextSize,
                                                                color: Theme.of(context)
                                                                    .accentColor,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 1,
                                                      height:
                                                          constraints.constrainHeight() * 0.6,
                                                      color: Theme.of(context).primaryColor,
                                                    ),
                                                    Expanded(
                                                      child: InkWell(
                                                        splashColor:
                                                            Theme.of(context).primaryColor,
                                                        customBorder: CircleBorder(),
                                                        onTap: () async {
                                                          await incrementTaskNumber(
                                                              isBalance: true);
                                                          showConfirmationSnackBar(
                                                              height: height,
                                                              text: "Rest Added");
                                                          //show store rating popup
                                                          askForRating(context);
                                                          setState(() {});
                                                        },
                                                        child: Column(
                                                          mainAxisSize: MainAxisSize.min,
                                                          children: [
                                                            Icon(
                                                              FontAwesomeIcons.arrowUp,
                                                              color: Theme.of(context)
                                                                  .primaryColor,
                                                              size: min(
                                                                      constraints
                                                                          .constrainHeight(),
                                                                      constraints
                                                                          .constrainWidth()) *
                                                                  0.4,
                                                            ),
                                                            SizedBox(
                                                              height: constraints
                                                                      .constrainHeight() *
                                                                  0.1,
                                                            ),
                                                            AutoSizeText(
                                                              "Rest",
                                                              style: TextStyle(
                                                                fontSize: kMainTextSize,
                                                                color: Theme.of(context)
                                                                    .accentColor,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: height * 0.0125,
                                      ),
                                    ],
                                  ),
                                  Visibility(
                                    visible: !userHasSeenBalance && tutorialStage != 2,
                                    child: BlurScreen(),
                                  ),
                                ],
                              ),
                              Stack(
                                children: [
                                  Column(
                                    children: [
                                      SizedBox(
                                        height: height * 0.0125,
                                      ),
                                      Align(
                                        child: Container(
                                          width: width * 0.9,
                                          height: height * 0.15,
                                          child: Card(
                                            elevation: 4.0,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(20),
                                            ),
                                            child: Padding(
                                              padding:
                                              EdgeInsets.symmetric(vertical: height * 0.03),
                                              child: LayoutBuilder(
                                                builder: (context, constraints) => Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                                  children: [
                                                    Expanded(
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          showDialog(
                                                            context: context,
                                                            builder: (context) =>
                                                                CheckInDialog(),
                                                          );
                                                          setState(() {
                                                            calculateBalance();
                                                            getBalanceBarColour(context);
                                                          });
                                                        },
                                                        child: Column(
                                                          mainAxisSize: MainAxisSize.min,
                                                          children: [
                                                            Icon(
                                                              FontAwesomeIcons.pencilAlt,
                                                              color: Theme.of(context)
                                                                  .primaryColor,
                                                              size: min(
                                                                  constraints
                                                                      .constrainHeight(),
                                                                  constraints
                                                                      .constrainWidth()) *
                                                                  0.4,
                                                            ),
                                                            SizedBox(
                                                              height: constraints
                                                                  .constrainHeight() *
                                                                  0.1,
                                                            ),
                                                            AutoSizeText(
                                                              "Check In",
                                                              style: TextStyle(
                                                                fontSize: kMainTextSize,
                                                                color: Theme.of(context)
                                                                    .accentColor,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 1,
                                                      height:
                                                      constraints.constrainHeight() * 0.6,
                                                      color: Theme.of(context).primaryColor,
                                                    ),
                                                    Expanded(
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          Navigator.pushNamed(
                                                              context, 'progress');
                                                        },
                                                        child: Column(
                                                          mainAxisSize: MainAxisSize.min,
                                                          children: [
                                                            Icon(
                                                              FontAwesomeIcons.chartBar,
                                                              color: Theme.of(context)
                                                                  .primaryColor,
                                                              size: min(
                                                                  constraints
                                                                      .constrainHeight(),
                                                                  constraints
                                                                      .constrainWidth()) *
                                                                  0.4,
                                                            ),
                                                            SizedBox(
                                                              height: constraints
                                                                  .constrainHeight() *
                                                                  0.1,
                                                            ),
                                                            AutoSizeText(
                                                              "Progress",
                                                              style: TextStyle(
                                                                fontSize: kMainTextSize,
                                                                color: Theme.of(context)
                                                                    .accentColor,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Visibility(
                                    visible: !userHasSeenBalance && tutorialStage != 3,
                                    child: BlurScreen(),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Visibility(
                            //here we will show the tutorial for section 1
                            visible: !userHasSeenBalance && tutorialStage != 2,
                            child: Stack(
                              children: [BlurScreen(), getTutorial(2)],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Visibility(
                    visible: !userHasSeenBalance && tutorialStage == 0,
                    child: BlurScreen(
                      child: BlossumExplanation(
                        onTap: (){
                          setState(() {
                            tutorialStage++;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  showConfirmationSnackBar({double height, String text}) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Theme.of(context).primaryColor,
        duration: Duration(seconds: 1),
        content: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: height * 0.05),
          child: AutoSizeText(
            text,
            maxLines: 1,
            style: TextStyle(color: Colors.white, fontSize: kMainTextSize),
          ),
        ),
      ),
    );
  }

  askForRating(BuildContext context){
    print(rateMyApp.shouldOpenDialog);
    if(rateMyApp.shouldOpenDialog){
      if(Theme.of(context).platform == TargetPlatform.iOS){
        rateMyApp.showStarRateDialog(context);
      } else {
        rateMyApp.showRateDialog(
            context,
            ignoreNativeDialog: true,
            title: "Rate Blossum",
            message: "Please take a moment to leave a review, it helps us improve!",
            rateButton: "RATE",
            noButton: "NO THANKS",
            laterButton: "MAYBE LATER",
            listener: (button) { // The button click listener (useful if you want to cancel the click event).
              switch (button) {
                case RateMyAppDialogButton.rate:
                  print('Clicked on "Rate".');
                  break;
                case RateMyAppDialogButton.later:
                  print('Clicked on "Later".');
                  break;
                case RateMyAppDialogButton.no:
                  print('Clicked on "No".');
                  break;
              }
              return true;
            });
      }
    }
}

  String createGreeting() {
    String returnString;
    String punctuation = ", \n";
    int hour = DateTime.now().hour;
    if (hour >= 0 && hour < 12) {
      returnString = "Good morning";
    } else if (hour < 18) {
      returnString = "Good afternoon";
    } else
      returnString = "Good evening";

    if(userGivenName == null){
      return returnString;
    } else {
      return returnString + punctuation + userGivenName;
    }
  }


  Widget getTutorial(int screenSection) {
    switch (tutorialStage) {
      case 1:
        if (screenSection == 1) {
          return null;
        } else if (screenSection == 2) {
          return BalanceTutorial(
            onTap: () {
              setState(() {
                tutorialStage++;
              });
            },
          );
        } else {
          return SizedBox();
        }
        break;
      case 2:
        if (screenSection == 1) {
          return WorkRestTutorial(
            onTap: () {
              setState(() {
                tutorialStage++;
              });
            },
          );
        } else if (screenSection == 2) {
          return SizedBox();
        } else {
          return SizedBox();
        }
        break;
      case 3:
        if (screenSection == 1) {
          return CheckInProgressTutorial(
            onTap: (){
              setState(() {
                tutorialStage = 1;
                userHasSeenBalance = true;
                finishBalanceTutorial();
              });
            },
          );
        } else if (screenSection == 2) {
          return SizedBox();
        } else {
          return SizedBox();
        }
        break;
      default:
        return SizedBox();
    }
  }

  bool userHasOpenedAppToday(){
    print(startOfDay(DateTime.now()).difference(startOfDay(userLastAppOpen)).inDays);
    return startOfDay(DateTime.now()).difference(startOfDay(userLastAppOpen)).inDays == 0;
  }

  Future<void> refreshTasks() async {
    HashMap<String, dynamic> userData = await getUserData();
    await assignUserData(userData, null, false);
    setState(() {});
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('MyApp state = $state');
    if (state == AppLifecycleState.inactive) {
      // app transitioning to other state.
    } else if (state == AppLifecycleState.paused) {
      // app is on the background.
    } else if (state == AppLifecycleState.detached) {
      // flutter engine is running but detached from views
    } else if (state == AppLifecycleState.resumed) {
      // app is visible and running.
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        print(DateTime.now().difference(userLastAppOpen).inHours);
        if(userLastAppOpen != null && userHasSeenBalance && DateTime.now().difference(userLastAppOpen).inHours > 1 && userHasOpenedAppToday()){
          await showDialog(
            context: context,
            builder: (context) => TimeAwayAlert(scaffoldKey: _scaffoldKey,),
          );
          setState(() {});
          logAppOpen();
          userLastAppOpen = DateTime.now();
        } else {
          logAppOpen();
          userLastAppOpen = DateTime.now();
        }
      });
    }
  }


}
