import 'dart:ui';

import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class BlossumExplanation extends StatelessWidget {
  final Function onTap;

  BlossumExplanation({
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        SizedBox(
          height: height * 0.03  ,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: Card(
            elevation: 4.0,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Theme.of(context).primaryColor, width: 1.5),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: width * 0.025, vertical: height * 0.02),
              child: Column(
                children: [
                  Text(
                    "Welcome to Blossum",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kMainTextSize,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "It's time to redefine productivity.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "Use me to track your work and rest tasks each day, whether it's ticking off steps towards your next big project or stopping to relax with some coffee.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "When you feel the need check in and tell me how you're feeling, I'll readjust your goals to help find you a work/rest balance that works for you, keeping you productive in a sustainable way, no more burnout!",

                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "I'll take the check in information you give me and create graphs for you to track all sorts of things, including your happiness and balance over time.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kSmallerTextSize,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  GestureDetector(
                    onTap: onTap,
                    child: Text(
                      "Let's Get Started",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: kMainTextSize
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}