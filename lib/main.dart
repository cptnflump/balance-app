import 'dart:collection';

import 'package:balance_app/screens/balance_screen.dart';
import 'package:balance_app/screens/delete_account.dart';
import 'package:balance_app/screens/go_pro_screen.dart';
import 'package:balance_app/screens/legal_screen.dart';
import 'package:balance_app/screens/manage_subscription_screen.dart';
import 'package:balance_app/screens/profile_screen.dart';
import 'package:balance_app/screens/progress_screen.dart';
import 'package:balance_app/screens/sign_in.dart';
import 'package:balance_app/screens/task_screen.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/push_notification_manager.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:purchases_flutter/purchases_flutter.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]);
  try{
    //TODO re add this after new firebase updates
//    await Firebase.initializeApp();
    PushNotificationsManager notificationsManager = PushNotificationsManager();
    notificationsManager.init();
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if(user != null){
      HashMap<String,dynamic> userData = await getUserData();
      await assignUserData(userData, null, true);
    }
  } catch (e){
    await FirebaseAuth.instance.signOut();
    wipeSessionData();
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        accentColor: kGrey,
        primaryColor: kOrange,
        fontFamily: 'Nunito',
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
          accentColor: kGreyWhite,
          primaryColor: kOrange,
          fontFamily: 'Nunito',
          brightness: Brightness.dark,
      ),
      initialRoute: userUid != null ? 'balance' : 'signIn',
      routes: {
        'tasks' : (context) => TaskScreen(),
        'signIn' : (context) => SignIn(),
        'profile' : (context) => ProfileScreen(),
        'balance' : (context) => BalanceScreen(),
        'progress' : (context) => ProgressScreen(),
        'subscription' : (context) => ManageSubscriptionScreen(),
        'goPro' : (context) => GoProScreen(),
        'deleteAccount' : (context) => DeleteAccountScreen(),
        'legalScreen' : (context) => LegalScreen(),
      },
    );
  }
}




