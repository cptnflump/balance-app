import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/task.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/alerts/task_alerts/task_modal.dart';
import 'package:flutter/material.dart';

class TaskOptionsModal extends StatefulWidget {

  final Task task;

  TaskOptionsModal({@required this.task});

  @override
  _TaskOptionsModalState createState() => _TaskOptionsModalState();
}

class _TaskOptionsModalState extends State<TaskOptionsModal> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.02),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  child: AutoSizeText(
                      "DELETE",
                    style: TextStyle(
                        fontSize: kMainTextSize,
                        color: Colors.red),
                  ),
                  onPressed: (){
                    removeItem();
                    },
                ),
                FlatButton(
                  child: AutoSizeText(
                      "EDIT",
                    style: TextStyle(
                      fontSize: kMainTextSize,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  onPressed: (){
                    processExistingTask(MediaQuery.of(context).size.height, widget.task);
                  },
                ),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
          ],
        ),
      );
  }

  void removeItem()async {
    userTasks.remove(widget.task);
    if(filteredUserTasks.contains(widget.task)) filteredUserTasks.remove(widget.task);
    await updateTasks();
    Navigator.pop(context);
    setState(() {});
  }

  void processExistingTask(double screenHeight, Task task) async {
    await showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: const Radius.circular(10), topRight: const Radius.circular(10)),
        ),
        context: context,
        builder: (context) => TaskModal(task: task,));
    Navigator.pop(context);
    setState(() {});
  }
}
