import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/screens/change_email_screen.dart';
import 'package:balance_app/screens/change_password_screen.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/string_handling.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/alerts/re_auth_alert.dart';
import 'package:balance_app/widgets/alerts/confirmation_dialog.dart';
import 'package:balance_app/widgets/navigation_bar.dart';
import 'package:balance_app/widgets/profile_row.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    String memberText = userIsSubscriber ? "Member Since" : "Started on";

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: Theme.of(context).primaryColor,
        title: AutoSizeText(
            "Profile",
          style: TextStyle(
            color: Colors.white
          ),
        ),
      ),
      bottomNavigationBar: NavigationBar(disableProfile: true,),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: height * 0.02, horizontal: width * 0.05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Visibility(
                      visible: userGivenName != null && userFamilyName != null,
                      child: AutoSizeText(
                        userGivenName == null || userFamilyName == null ? "" : userGivenName + " " + userFamilyName,
                        maxLines: 1,
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: kTitleTextSize,
                        ),
                      ),
                    ),
                    AutoSizeText(
                      userIsSubscriber ? "Premium Member" : "Free User",
                      maxLines: 1,
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontSize: kSmallTextSize
                      ),
                    ),
                    AutoSizeText(
                      userJoinedOn == null ? "" : memberText + " " + formatDate(userJoinedOn) + " " + userJoinedOn.year.toString(),
                      maxLines: 1,
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontSize: kSmallTextSize,
                      ),
                    ),
                  ],
                ),
              ),
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: userIsSubscriber ? FontAwesomeIcons.creditCard : FontAwesomeIcons.spa,
                rowText: userIsSubscriber ? "Manage Subscription" : "Go Pro",
                onTap: (){
                  userIsSubscriber ? Navigator.pushNamed(context, 'subscription') : Navigator.pushNamed(context, "goPro");
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Divider(
                  color: Theme.of(context).primaryColor,
                  thickness: 1,
                ),
              ),
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: FontAwesomeIcons.eraser,
                rowText: "Reset Completed Tasks",
                onTap: () async{
                  if(userIsSubscriber){
                    await showDialog(context: context, builder: (context) => ConfirmationDialog(
                      actionDesc: "reset your completed tasks",
                      reversible: false,
                      yesAction: (){
                        userLifetimeCompletedTasks = 0;
                        userNumberCompletedBalance = 0;
                        resetTaskTotals();
                        Navigator.pop(context);
                      },
                    ));
                  } else Navigator.pushNamed(context, 'goPro');
                  setState(() {});
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Divider(
                  color: Theme.of(context).primaryColor,
                  thickness: 1,
                ),
              ),
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: FontAwesomeIcons.chalkboardTeacher,
                rowText: "Reset Tutorials",
                onTap: () async{
                    await showDialog(context: context, builder: (context) => ConfirmationDialog(
                      actionDesc: "reset your completed tutorials",
                      reversible: false,
                      yesAction: (){
                        resetTutorials();
                        userHasCreatedTask = false;
                        userHasSeenTask = false;
                        userHasSeenBalance = false;
                        Navigator.pop(context);
                      },
                    ));
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Divider(
                  color: Theme.of(context).primaryColor,
                  thickness: 1,
                ),
              ),

              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: FontAwesomeIcons.solidEnvelope,
                rowText: "Change Email",
                onTap: () async{
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ChangeEmailScreen(scaffoldKey),
                  ));
//                  await showDialog(context: context, builder: (context) => ChangeEmailAlert(scaffoldKey));
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Divider(
                  color: Theme.of(context).primaryColor,
                  thickness: 1,
                ),
              ),
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: FontAwesomeIcons.lock,
                rowText: "Change Password",
                onTap: () async{
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ChangePasswordScreen(scaffoldKey),
                  ));
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Divider(
                  color: Theme.of(context).primaryColor,
                  thickness: 1,
                ),
              ),
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: FontAwesomeIcons.fileContract,
                rowText: "Legal",
                onTap: (){
                  Navigator.pushNamed(context, 'legalScreen');
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Divider(
                  color: Theme.of(context).primaryColor,
                  thickness: 1,
                ),
              ),
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: FontAwesomeIcons.doorOpen,
                rowText: "Sign Out",
                onTap: (){
                  showDialog(
                      context: context,
                      builder: (context){
                        return ConfirmationDialog(
                          yesAction: (){
                            signOut(context);
                          },
                          actionDesc: "sign out",
                          reversible: true,
                        );
                      }
                  );
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Divider(
                  color: Theme.of(context).primaryColor,
                  thickness: 1,
                ),
              ),
              ProfileRow(
                screenHeight: height,
                screenWidth:  width,
                icon: FontAwesomeIcons.userAltSlash,
                rowText: "Delete Account",
                onTap: (){
                  Navigator.pushNamed(context, 'deleteAccount');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}



reAuthAndDelete(BuildContext context) async {
 FirebaseUser user = await FirebaseAuth.instance.currentUser();
  String provider = user.providerId;
  if(provider == "google.com"){
    final GoogleSignIn _googleSignIn = GoogleSignIn();
    await _googleSignIn.currentUser.authentication;
    deleteAccount(context);
  } else {
    showDialog(
        context: context,
        builder: (context){
          return ReAuthAlert();
        });
  }
}


