import 'package:balance_app/utilities/project.dart';

class Task{

  Task({
    this.taskTitle,
    this.taskDue,
    this.isPriority,
    this.isBalance,
    this.isComplete,
    this.project,
});

  String taskTitle;
  DateTime taskDue;
  bool isPriority;
  bool isBalance;
  bool isComplete;
  String project;
}