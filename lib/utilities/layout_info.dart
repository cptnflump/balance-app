import 'package:flutter/cupertino.dart';
import 'dart:math';

class LayoutInfo{
  double height;
  double width;
  double smallestDimension;
  Widget child;

  LayoutInfo({@required BuildContext context, @required this.child}){
    LayoutBuilder(
      builder: (context, constraints){
        this.height = constraints.maxHeight;
        this.width = constraints.maxWidth;
        this.smallestDimension = min(height,width);
        return null;
      },
    );
  }

  Widget buildScreen(){
    return child;
  }


}