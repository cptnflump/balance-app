import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/balance_handler.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/string_handling.dart';
import 'package:balance_app/utilities/time_handler.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/image_widgets/sized_icon.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CheckInDialog extends StatefulWidget {

  @override
  _CheckInDialogState createState() => _CheckInDialogState();
}

class _CheckInDialogState extends State<CheckInDialog> {

  bool setManually = false;
  int sliderValue = userGoal.round();

  Color face1Colour;
  Color face2Colour;
  Color face3Colour;
  Color face4Colour;
  Color face5Colour;

  int happiness;

  @override
  Widget build(BuildContext context) {

    if(face1Colour == null) face1Colour = Theme.of(context).accentColor.withOpacity(0.8);
    if(face2Colour == null) face2Colour = Theme.of(context).accentColor.withOpacity(0.8);
    if(face3Colour == null) face3Colour = Theme.of(context).accentColor.withOpacity(0.8);
    if(face4Colour == null) face4Colour = Theme.of(context).accentColor.withOpacity(0.8);
    if(face5Colour == null) face5Colour = Theme.of(context).accentColor.withOpacity(0.8);

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    int roundedGoal = userGoal.round();
    int roundedBalance = userBalance.round();
    String lastCheckIn = userLastCheckIn == null ? null : formatDate(userLastCheckIn);

    String lastCheckInText = userLastCheckIn == null ?
      "Looks like you haven't checked in before!" :
      "Your last check up was on\n$lastCheckIn";

    int checkInGap = userIsSubscriber ? 1 : 7;

    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kAlertBorderRadius),
      ),
      content: Container(
          width: width * 0.9,
          child: userLastCheckIn == null || startOfDay(DateTime.now()).difference(startOfDay(userLastCheckIn)).inDays >= checkInGap ?
          buildCheckInAlert(lastCheckInText, height, roundedGoal, roundedBalance, width, context) :
          buildInvalidCheckIn(height: height, width: width, checkInGap: checkInGap),
      ),
    );
  }

  Column buildCheckInAlert(String lastCheckInText, double height, int roundedGoal, int roundedBalance, double width, BuildContext context) {
    return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AutoSizeText(
              lastCheckInText,
              maxLines: 2,
              style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
            ),
            SizedBox(height: height * 0.02,),
            AutoSizeText(
              "Your goal balance was $roundedGoal% and you currently have a balance of $roundedBalance%",
              maxLines: 2,
              style: TextStyle(
                fontSize: kSmallerTextSize,
                color: Theme.of(context).accentColor,
              ),
            ),
            SizedBox(height: height * 0.02,),
            AutoSizeText(
              "How do you feel?",
              maxLines: 2,
              style: TextStyle(
                fontSize: kSmallerTextSize,
                color: Theme.of(context).accentColor,
              ),
            ),
            SizedBox(height: height * 0.03,),
            Container(
              width: width * 0.9,
              height: height * 0.1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  GestureDetector(
                      child: SizedIcon(
                        icon: FontAwesomeIcons.sadCry,
                        parentChildRatio: 0.6,
                        colour: face1Colour,
                      ),
                    onTap: (){
                        setState(() {
                          happiness = 1;
                          if(face1Colour == Theme.of(context).accentColor.withOpacity(0.8)) face1Colour = kDarkRed;
                          face2Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face3Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face4Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face5Colour = Theme.of(context).accentColor.withOpacity(0.8);
                        });
//                      adjustBalance(happiness: 1, context: context);
                    },
                  ),
                  GestureDetector(
                      child: SizedIcon(
                        icon: FontAwesomeIcons.frown,
                        parentChildRatio: 0.6,
                        colour: face2Colour,
                      ),
                    onTap: (){
                        setState(() {
                          happiness = 2;
                          face1Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          if(face2Colour == Theme.of(context).accentColor.withOpacity(0.8)) face2Colour = kLightRed;
                          face3Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face4Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face5Colour = Theme.of(context).accentColor.withOpacity(0.8);
                        });
//                      adjustBalance(happiness: 2, context: context);
                    },
                  ),
                  GestureDetector(
                      child: SizedIcon(
                        icon: FontAwesomeIcons.meh,
                        parentChildRatio: 0.6,
                        colour: face3Colour,
                      ),
                    onTap: (){
                        setState(() {
                          happiness = 3;
                          face1Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face2Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          if(face3Colour == Theme.of(context).accentColor.withOpacity(0.8)) face3Colour = Theme.of(context).primaryColor;
                          face4Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face5Colour = Theme.of(context).accentColor.withOpacity(0.8);
                        });
//                        adjustBalance(happiness: 3, context: context);
                    },
                  ),
                  GestureDetector(
                      child: SizedIcon(
                        icon: FontAwesomeIcons.smile,
                        parentChildRatio: 0.6,
                        colour: face4Colour,
                      ),
                    onTap: (){
                        setState(() {
                          happiness = 4;
                          face1Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face2Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face3Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          if(face4Colour == Theme.of(context).accentColor.withOpacity(0.8)) face4Colour = kMustard;
                          face5Colour = Theme.of(context).accentColor.withOpacity(0.8);
                        });
//                        adjustBalance(happiness: 4, context: context);
                    },
                  ),
                  GestureDetector(
                      child: SizedIcon(
                        icon: FontAwesomeIcons.smileBeam,
                        parentChildRatio: 0.6,
                        colour: face5Colour,
                      ),
                    onTap: (){
                        setState(() {
                          happiness = 5;
                          face1Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face2Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face3Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          face4Colour = Theme.of(context).accentColor.withOpacity(0.8);
                          if(face5Colour == Theme.of(context).accentColor.withOpacity(0.8)) face5Colour = kLightGreen;
                        });
//                        adjustBalance(happiness: 5, context: context);
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: height * 0.02,),
            Visibility(
              visible: !setManually,
              child: happiness == null ?
              Center(
                child: Text(
                  "Please select a happiness to continue",
                  style: TextStyle(
                    fontSize: kSmallTextSize,
                    color: Theme.of(context).primaryColor,
                  ),
                  textAlign: TextAlign.center,
                ),
              ) :
              GestureDetector(
                onTap: (){
                  if(happiness != null) adjustBalance(happiness: happiness, context: context);
                },
                child: Center(
                  child: AutoSizeText(
                    "Continue",
                    maxLines: 1,
                    style: TextStyle(color: Theme.of(context).primaryColor, fontSize: kMainTextSize),
                  ),
                ),
              ),
            ),
            SizedBox(height: height * 0.02,),
            Visibility(
              visible: !setManually,
              child: GestureDetector(
                onTap: (){
                  if(userIsSubscriber){
                    setState(() {
                      setManually = !setManually;
                    });
                  }
                  else {
                    Navigator.pushNamed(context, 'goPro');
                  }
                },
                child: Center(
                  child: AutoSizeText(
                    "Set Balance Manually",
                    maxLines: 1,
                    style: TextStyle(color: Theme.of(context).accentColor, fontSize: kSmallerTextSize),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: setManually,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AutoSizeText(
                    "How much of your day do you want to be working?",
                    maxLines: 2,
                    style: TextStyle(color: Theme.of(context).accentColor, fontSize: kSmallerTextSize),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: height * 0.03,),
                  Row(
                    textBaseline: TextBaseline.alphabetic,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: [
                      Text(
                        sliderValue.toString(),
                        maxLines: 1,
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: kMainTextSize
                        ),
                      ),
                      SizedBox(width: width * 0.005,),
                      Text(
                        "%",
                        maxLines: 1,
                        style: TextStyle(
                          color: Theme.of(context).accentColor, fontSize: kSmallerTextSize,
                        ),
                      ),
                    ],
                  ),
                  Slider(
                    min: 0,
                    max: 100,
                    value: sliderValue.toDouble(),
                    activeColor: Theme.of(context).primaryColor,
                    inactiveColor: Theme.of(context).accentColor.withOpacity(0.2),
                    onChanged: (double value) {
                      setState(() {
                        sliderValue = value.round();
                      });
                    },
                  ),
                  SizedBox(
                    height: height * 0.01,
                  ),
                  Visibility(
                    visible: happiness == null,
                    child: AutoSizeText(
                      "Please select a happiness before saving!",
                      style: TextStyle(color: Theme.of(context).accentColor, fontSize: kSmallerTextSize),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        child: AutoSizeText(
                          "Cancel",
                          maxLines: 1,
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        onTap: (){
                          setState(() {
                            setManually = !setManually;
                          });
                        },
                      ),
                      GestureDetector(
                        onTap: (){
                          if(happiness != null) saveManualGoal(sliderValue, happiness);
                        },
                        child: AutoSizeText(
                          "Save",
                          maxLines: 1,
                          style: TextStyle(
                            color: happiness == null ? Theme.of(context).accentColor : Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        );
  }

  Column buildInvalidCheckIn({double height, double width, int checkInGap}){

    int daysUntilCheckIn = checkInGap + startOfDay(userLastCheckIn).difference(startOfDay(DateTime.now())).inDays;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        AutoSizeText(
          "Sit Tight!",
          style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: height * 0.04,),
        AutoSizeText(
          userIsSubscriber ? "As a premium member you can check in once per day!" : "As a free user you have to wait a week between each check in.",
          style: TextStyle(
                fontSize: kSmallerTextSize,
                color: Theme.of(context).accentColor,
              ),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: height * 0.04,),
        AutoSizeText(
          daysUntilCheckIn != 1 ? "Come back in $daysUntilCheckIn days to check in" + (userIsSubscriber ? "." : "\nor go pro to be able to check in daily.") :  "Come back tomorrow to check in.",
          style: TextStyle(
                fontSize: kSmallerTextSize,
                color: Theme.of(context).accentColor,
              ),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: height * 0.04,),
        Row(
          mainAxisAlignment: userIsSubscriber ? MainAxisAlignment.end : MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              child: AutoSizeText(
                "Okay",
                maxLines: 1,
                style: TextStyle(
                  fontSize: kMainTextSize,
                  color: userIsSubscriber ? Theme.of(context).primaryColor : Theme.of(context).accentColor,
                ),
              ),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            userIsSubscriber ? SizedBox() : GestureDetector(
              onTap: (){
                Navigator.pushNamed(context, 'goPro');
              },
              child: AutoSizeText(
                "Go Pro",
                maxLines: 1,
                style: TextStyle(
                  fontSize: kMainTextSize,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  saveManualGoal(int newGoal, int happiness){
    updateCheckInList(happiness);
    userGoal = 100 - newGoal.toDouble();
    updateGoal();
    userNumberCompletedBalance = 0;
    userNumberCompletedTasks = 0;
    userBalance = 50;
    userLastCheckIn = DateTime.now();
    Navigator.popUntil(context, ModalRoute.withName('balance'));
    setState(() {});
  }
}
