import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class ErrorImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset('images/error.png'),
        SizedBox(height: 20,),
        AutoSizeText(
          "Oh no! Looks like something went wrong...",
          style: TextStyle(
                fontSize: kHugeTextSize,
                fontWeight: FontWeight.bold,
                color: Theme.of(context).accentColor,
              ),
          maxLines: 1,
        ),
      ],
    );
  }
}
