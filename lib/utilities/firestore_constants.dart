//CONSTANTS REGARDING TABLES
const fbCompletedTasksTable = "CompletedTasks";
const fbUsersTable = 'Users';
const fbCheckInTable = 'CheckIns';

//CONSTANTS REGARDING USERS
const fbTasksField = 'tasks';
const fbUserEmail = "email";
const fbUserGoal = "goal";
const fbUserBalance = "balance";
const fbUserCompletedTasks = "completedTasks";
const fbLifetimeCompletedTasks = "lifetimeWorkTasks";
const fbLifeTimeBalanceTasks = "lifetimeBalanceTasks";
const fbUserCompletedBalance = "balanceTasks";
const fbUserProjects = "projects";
const fbUserLastCheckIn = "lastCheckIn";
const fbUserGivenName = 'givenName';
const fbUserFamilyName = 'familyName';
const fbUserJoinedOn = "created";
const fbUserHasSeenBalance = "hasSeenBalance";
const fbUserHasCreatedTask = "hasCreatedTask";
const fbUserHasSeenTask = "hasSeenTask";
const fbLastAppOpen = "lastAppOpen";

//CONSTANTS REGARDING CHECK INS TABLE
const fbCheckInList = "checkIns";
const fbCheckInDate = "date";
const fbCheckInGoal = "goal";
const fbCheckInBalancePercentage = "balancePercent";
const fbCheckInTasksCompleted = "workTasks";
const fbCheckInBalanceCompleted = "balanceTasks";
const fbCheckInHappiness = "happiness";


//TASK CONSTANT VALUES
const fbUid = 'uid';
const fbTaskDue = 'taskDue';
const fbTaskTitle = 'taskTitle';
const fbIsPriority = 'isPriority';
const fbSubscriber = 'subscriber';
const fbJoinedOn = 'created';
const fbIsComplete = 'isComplete';
const fbGivenName = "givenName";
const fbFamilyName = "familyName";
const fbTaskProject = "project";
const fbIsBalance = 'isBalance';
