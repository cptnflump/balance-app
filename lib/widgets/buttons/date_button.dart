import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class DateButton extends StatefulWidget {
  const DateButton({
    @required this.width,
    @required this.text,
    @required this.boxColour,
    @required this.onTap,
  });

  final double width;
  final String text;
  final Color boxColour;
  final Function onTap;

  @override
  _DateButtonState createState() => _DateButtonState();
}

class _DateButtonState extends State<DateButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
          decoration: BoxDecoration(
            color: widget.boxColour,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: widget.width * 0.015),
            child: AutoSizeText(widget.text, style: TextStyle(color: widget.boxColour == Theme.of(context).primaryColor ? Colors.white : Theme.of(context).accentColor),),
          )
      ),
      onTap: widget.onTap,
    );
  }
}