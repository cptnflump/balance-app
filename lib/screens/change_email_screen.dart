import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/image_widgets/loading_circle.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

enum Provider{
  apple,
  google,
  password
}

class ChangeEmailScreen extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  ChangeEmailScreen(this.scaffoldKey);

  @override
  _ChangeEmailScreenState createState() => _ChangeEmailScreenState(scaffoldKey);
}

class _ChangeEmailScreenState extends State<ChangeEmailScreen> {
  
  final GlobalKey<ScaffoldState> scaffoldKey;
  final _formKey = GlobalKey<FormState>();
  String errorMessage;
  Future<Provider> currProvider;

  _ChangeEmailScreenState(this.scaffoldKey);

  TextEditingController passwordController = TextEditingController();
  TextEditingController newEmailController = TextEditingController();

  @override
  void initState() {
    super.initState();
    currProvider = checkCurrProvider();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return FutureBuilder(
      future: currProvider,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Container(); //todo here add an error message
          } else {
            print(snapshot.data);
              return Scaffold(
                appBar: AppBar(
                  backgroundColor: Theme.of(context).primaryColor,
                  iconTheme: IconThemeData(
                    color: Colors.white,
                  ),
                  title: Text(
                    "Change Email",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: kTitleTextSize
                    ),
                  ),
                ),
                body: Form(
                  key: _formKey,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: height * 0.02,),
                        Text(
                          "Change Email",
                          style: TextStyle(
                fontSize: kTitleTextSize,
                color: Theme.of(context).accentColor,
              ),
                        ),
                        Visibility(
                          visible: errorMessage != null,
                          child: AutoSizeText(
                            errorMessage == null ? "" : errorMessage,
                            style: TextStyle(
                                color: Colors.red, fontSize: kSmallerTextSize),
                          ),
                        ),
                        SizedBox(height: height * 0.02,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              "New Email",
                              style: TextStyle(
                                fontSize: kSmallerTextSize,
                                color: Theme.of(context).accentColor,
                              ),
                            ),
                            TextFormField(
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration(
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: kOrange),
                                ),
                              ),
                              style: TextStyle(fontSize: kSmallerTextSize),
                              controller: newEmailController,
                              obscureText: false,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Please Enter Some Text";
                                } else
                                  return null;
                              },
                            )
                          ],
                        ),
                        snapshot.data == Provider.password ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              "Password",
                              style: TextStyle(
                                fontSize: kSmallerTextSize,
                                color: Theme.of(context).accentColor,
                              ),
                            ),
                            TextFormField(
                              cursorColor: Theme.of(context).primaryColor,
                              style: TextStyle(fontSize: kSmallerTextSize),
                              decoration: InputDecoration(
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: kOrange),
                                ),
                              ),
                              controller: passwordController,
                              obscureText: true,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Please Enter Some Text";
                                } else
                                  return null;
                              },
                            )
                          ],
                        ) : SizedBox(),
                        SizedBox(height: height * 0.02,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                              splashColor: Theme.of(context).primaryColor.withOpacity(0.5),
                              child: Text(
                                "Cancel",
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: kSmallTextSize
                                ),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            FlatButton(
                              splashColor: Theme.of(context).primaryColor.withOpacity(0.5),
                              child: Text(
                                "Save",
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: kSmallTextSize
                                ),
                              ),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  String newEmail = newEmailController.text
                                      .trim();
                                  String pass = passwordController.text.trim();
                                  if(snapshot.data == Provider.password){
                                    reAuthAndChangeEmail(context: context,
                                        newEmail: newEmail,
                                        pass: pass);
                                  } else if(snapshot.data == Provider.google){
                                    reAuthWithGoogle(newEmail);
                                  } else {
                                    reAuthWithApple(newEmail);
                                  }
                                }
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
          }
        } else {
          return Scaffold(body: SafeArea(child: Center(child: LoadingCircle(width: width,))));
        }
      });
  }

  bool emailIsNew(FirebaseUser user, String newEmail){
    return user.email != newEmail;
  }

  /// This returns a string which is an error message, if the error message is null there was no error.
  reAuthAndChangeEmail({BuildContext context, String newEmail, String pass}) async{
   FirebaseUser user = await FirebaseAuth.instance.currentUser();

   if(emailIsNew(user, newEmail)){
     if(errorMessage != null){
       AuthCredential credential = EmailAuthProvider.getCredential(
           email: user.email,
           password: pass
       );

       await user.reauthenticateWithCredential(credential).catchError((error){
         print(error);
         errorMessage = "Password Does Not Match.";
       });

       updateEmail(user, newEmail);
     }
   } else {
     errorMessage = "Email must be different than your current email";
   }

  }

  reAuthWithGoogle(String newEmail) async{
   FirebaseUser user = await FirebaseAuth.instance.currentUser();
   if(emailIsNew(user, newEmail)){
     if(errorMessage != null){
       final GoogleSignIn _googleSignIn = GoogleSignIn();
       final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
       final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

       final AuthCredential credential = GoogleAuthProvider.getCredential(
         accessToken: googleAuth.accessToken,
         idToken: googleAuth.idToken,
       );
       await FirebaseAuth.instance.signInWithCredential(credential);

       updateEmail(user, newEmail);
     }
   } else {
     errorMessage = "Email must be different than your current email";
   }


  }

  reAuthWithApple(String newEmail) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    if(emailIsNew(user, newEmail)){
      final AuthorizationResult result = await AppleSignIn.performRequests([
        AppleIdRequest()
      ]);
      switch (result.status) {
        case AuthorizationStatus.authorized:
          print("successfull sign in");
          final AppleIdCredential appleIdCredential = result.credential;

          String givenName = appleIdCredential.fullName.givenName;
          String familyName = appleIdCredential.fullName.familyName;
          userGivenName = givenName;
          userFamilyName = familyName;

          OAuthProvider oAuthProvider =
          new OAuthProvider(providerId: "apple.com");
          final AuthCredential credential = oAuthProvider.getCredential(
            idToken:
            String.fromCharCodes(appleIdCredential.identityToken),
            accessToken:
            String.fromCharCodes(appleIdCredential.authorizationCode),
          );

          await FirebaseAuth.instance.signInWithCredential(credential);
          FirebaseUser user = await FirebaseAuth.instance.currentUser();

          updateEmail(user, newEmail);

          break;
        case AuthorizationStatus.error:
          print("Error with Apple Sign in");
          break;

        case AuthorizationStatus.cancelled:
          print('User cancelled');
          break;
      }
    } else {
      setState(() {
        errorMessage = "Email must be different than your current email";
      });
    }



  }


  //changing email:
  //check current platform
  //if on android check if there is a google provider and auth with google
  //if on android and no google provider auth with password
  //if on iOS check if there is an apple provider and auth with apple
  //if on iOS and no apple provider check if can auth with google
  // if on iOS and neither providers present auth with password
  Future<Provider> checkCurrProvider() async{
   FirebaseUser user = await FirebaseAuth.instance.currentUser();

    List<String> providers = [];
    print(user.providerId);
    print("");
    user.providerData.forEach((element) {
      if(!providers.contains(element.providerId)) providers.add(element.providerId);
    });
    print(providers);

    TargetPlatform platform = Theme.of(context).platform;
    if(platform == TargetPlatform.android){
      if(providers.contains("google.com")){
        return Provider.google;
      } else {
        return Provider.password;
      }
    } else {
      setState(() {});
      if(providers.contains("apple.com")){
        return Provider.apple;
      } else if(providers.contains("google.com")){
        return Provider.google;
      } else {
        return Provider.password;
      }
    }
  }

  updateEmail(FirebaseUser user, String newEmail) async{
    if(errorMessage == null){
      try{
        await user.updateEmail(newEmail);
        Navigator.pop(context);
        scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Email Changed", style: TextStyle(color: Colors.white),), backgroundColor: Theme.of(context).primaryColor,));
      } catch (error) {
        print(error);
        errorMessage = "New Email is not valid.";
        setState(() {});
      }
    } else {
      setState(() {
      });
    }
  }


  @override
  void dispose() {
    newEmailController.dispose();
    super.dispose();
  }
}
