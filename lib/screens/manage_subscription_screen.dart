import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/firestore_handler.dart';
import 'package:balance_app/utilities/time_handler.dart';
import 'package:flutter/material.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

class ManageSubscriptionScreen extends StatefulWidget {
  @override
  _ManageSubscriptionScreenState createState() => _ManageSubscriptionScreenState();
}

class _ManageSubscriptionScreenState extends State<ManageSubscriptionScreen> {
  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: Theme.of(context).primaryColor,
        title: AutoSizeText(
          "Manage Subscription",
          style: TextStyle(
              color: Colors.white
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(left: width * 0.1, right: width * 0.1, top: height * 0.05 ),
          child: FutureBuilder(
            future: getPurchaseInfo(),
            builder: (context, snapshot) {
              if(snapshot.connectionState == ConnectionState.done){
                if(snapshot.hasError){
                  print(snapshot.error);
                  return SizedBox();
                } else {
                  if(snapshot.data == null){
                     return Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         Text(
                           "You have been manually upgraded as a subscriber, so you shouldn't be billed!",
                           style: TextStyle(
                             fontSize: kMainTextSize,
                             color: Theme.of(context).accentColor,
                           ),
                         ),
                         SizedBox(height: height * 0.03,),
                         Text(
                           "Need to get in touch?",
                           style: TextStyle(
                             fontSize: kMainTextSize,
                             color: Theme.of(context).accentColor,
                           ),
                         ),
                         SizedBox(height: height * 0.015,),
                         Text(
                           "Send us an email at\nblossum-app@youfluent.co.uk",
                           style: TextStyle(
                             fontSize: kSmallTextSize,
                             color: Theme.of(context).accentColor,
                           ),
                         ),
                       ],
                     );
                  } else {
                    PurchaserInfo info = snapshot.data[0];
                    EntitlementInfo subDetails = snapshot.data[1];
                    Product product = snapshot.data[2];
                    print(subDetails);
                    print(product);
                    getExpirationDate(subDetails);
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Pro Subscription",
                          style: TextStyle(
                              fontSize: kTitleTextSize,
                              color: Theme.of(context).accentColor
                          ),
                        ),
                        SizedBox(height: height * 0.03,),
                        Text(
                          subDetails.willRenew ? "Your next bill will be " + product.priceString + " on " + getExpirationDate(subDetails) : "Subscription ending on " + getExpirationDate(subDetails),
                          style: TextStyle(
                              fontSize: kMainTextSize,
                              color: Theme.of(context).accentColor
                          ),
                        ),
                        SizedBox(height: height * 0.03,),
                        Text(
                          "Want to cancel your subscription?",
                          style: TextStyle(
                              fontSize: kMainTextSize,
                              color: Theme.of(context).accentColor
                          ),
                        ),
                        SizedBox(height: height * 0.015,),
                        Text(
                          Theme.of(context).platform == TargetPlatform.iOS ?
                          "Tap on your name in your settings screen to find your subscriptions, where you will be able to cancel your subscription." :
                          "In the google play store, open the menu and tap subscriptions, where you will be able to select Blossum and cancel your subscription.",
                          style: TextStyle(
                            fontSize: kSmallTextSize,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        SizedBox(height: height * 0.03,),
                        Text(
                          "Need to get in touch?",
                          style: TextStyle(
                            fontSize: kMainTextSize,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        SizedBox(height: height * 0.015,),
                        Text(
                          "Send us an email at\nblossum-app@youfluent.co.uk",
                          style: TextStyle(
                            fontSize: kSmallTextSize,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                      ],
                    );
                  }

                }
              } else {
                return SizedBox();
              }
            },
          ),
        ),
      ),
    );
  }

  Future<List<dynamic>> getPurchaseInfo() async {
    bool isOverride = await forcedSubscriber();
    if(isOverride){
      return null;
    } else {
      List<dynamic> toReturn = [];
      PurchaserInfo info = await Purchases.getPurchaserInfo();
      EntitlementInfo subDetails = info.entitlements.active["Pro"];
      Product product = await Purchases.getProducts([subDetails.productIdentifier]).then((value) => value[0]);
      toReturn.add(info);
      toReturn.add(subDetails);
      toReturn.add(product);
      return toReturn;
    }
  }

  String getExpirationDate(EntitlementInfo entitlementInfo){
    String dateString = entitlementInfo.expirationDate.split("T")[0];
    List<String> dateParts = dateString.split("-");

    String month = dateParts[1];
    month = getMonth(int.parse(month));

    String day = dateParts[2];
    day = day + getDaySuffix(int.parse(day));

    return(month + " " + day);
  }

}
