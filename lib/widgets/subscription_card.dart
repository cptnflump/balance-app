import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class SubscriptionCard extends StatefulWidget {
  final String price;
  final String desc;
  final bool isMonthly;
  final bool isActive;
  final Function onTap;

  SubscriptionCard({
    @required this.price,
    @required this.desc,
    @required this.isMonthly,
    @required this.isActive,
    @required this.onTap,
  });

  @override
  _SubscriptionCardState createState() => _SubscriptionCardState(price, desc, isMonthly, isActive, onTap);
}

class _SubscriptionCardState extends State<SubscriptionCard> {
  final String price;
  final String desc;
  final bool isMonthly;
  final Function onTap;
  bool isActive = false;

  _SubscriptionCardState(
      this.price,
      this.desc,
      this.isMonthly,
      this.isActive,
      this.onTap
  );



  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.transparent,
          border: Border.all(color: kOrange),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: height * 0.02),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                color: Colors.transparent,
              ),
                child: Text(
                  isMonthly ? "Monthly" : "Yearly",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: kMainTextSize,
                    color: Theme.of(context).accentColor
                  ),
                ),
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: height * 0.02),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(bottomRight: Radius.circular(20), bottomLeft: Radius.circular(20)),
                color: isActive ? Theme.of(context).primaryColor : Colors.transparent,
              ),
              child: Column(
                children: [
                  Text(
                    price,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: kMainTextSize,
                      color: isActive ? Colors.white : Theme.of(context).accentColor,
                    ),
                  ),
                  SizedBox(height: height * 0.01,),
                  Text(
                    monthlyCost(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: kSmallTextSize,
                        color: isActive ? Colors.white : Theme.of(context).accentColor,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String monthlyCost(){
    if(isMonthly){
      return price + "/mo";
    } else {
      return "£" + (double.parse(price.substring(1)) / 12).toStringAsFixed(2) + "/mo";
    }
  }
}