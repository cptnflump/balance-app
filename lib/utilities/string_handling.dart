String capitalize(String string) {
  if (string == null) {
    throw ArgumentError("string: $string");
  }

  if (string.isEmpty) {
    return string;
  }

  return string[0].toUpperCase() + string.substring(1);
}

String formatDate(DateTime dateTime){
  int month = dateTime.month;
  int day = dateTime.day;
  String monthString;
  switch (month){
    case 1:
      monthString = "January";
      break;
    case 2:
      monthString = "February";
      break;
    case 3:
      monthString = "March";
      break;
    case 4:
      monthString = "April";
      break;
    case 5:
      monthString = "May";
      break;
    case 6:
      monthString = "June";
      break;
    case 7:
      monthString = "July";
      break;
    case 8:
      monthString = "August";
      break;
    case 9:
      monthString = "September";
      break;
    case 10:
      monthString = "October";
      break;
    case 11:
      monthString = "November";
      break;
    case 12:
      monthString = "December";
      break;
  }
  String daySuffix;
  if(day.toString().endsWith("1")){
    daySuffix = "st";
  } else if(day.toString().endsWith("2")){
    daySuffix = "nd";
  } else if(day.toString().endsWith("3")) {
    daySuffix = "rd";
  } else {
    daySuffix = "th";
  }

  return monthString + " " + day.toString() + daySuffix;
}