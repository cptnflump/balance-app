import 'package:auto_size_text/auto_size_text.dart';
import 'package:balance_app/utilities/constants.dart';
import 'package:balance_app/utilities/user_data.dart';
import 'package:balance_app/widgets/subscription_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:purchases_flutter/purchases_flutter.dart';


class GoProScreen extends StatefulWidget {

  @override
  _GoProScreenState createState() => _GoProScreenState();
}

class _GoProScreenState extends State<GoProScreen> {
  bool monthlySelected = false;
  bool yearlySelected = true;

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    isSubscriber();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Upgrade",
          style: TextStyle(
            color: Colors.white,
            fontSize: kTitleTextSize
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: height * 0.05, horizontal: width * 0.05),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      textBaseline: TextBaseline.alphabetic,
                      children: [
                        Expanded(
                            child: Image.asset(
                                Theme.of(context).brightness == Brightness.light ? "images/blossum.png" : "images/blossum_dark.png",
                            ),
                        ),
                        SizedBox(width: width * 0.01,),
                        Text(
                          "Pro",
                          style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
                        ),
                      ],
                    ),
                  ),
                  FeatureRow(
                    text: "Daily check ins",
                  ),
                  FeatureRow(
                    text: "Manual goal control",
                  ),
                  FeatureRow(
                    text: "Graph your progress",
                  ),
                  FeatureRow(
                    text: "Track your happiness",
                  ),
                  FeatureRow(
                    text: "Lifetime task resets",
                  ),
                ],
              ),
              FutureBuilder(
                future: Purchases.getOfferings(),
                builder: (context, snapshot){
                  if(snapshot.connectionState == ConnectionState.done){
                    Offerings offerings = snapshot.data as Offerings;
                    if(snapshot.hasError || offerings.current == null){
                      return GestureDetector(
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: height * 0.03),
                          child: Container(
                            width: width * 0.8,
                            padding: EdgeInsets.symmetric(vertical: height * 0.015),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(color: Theme.of(context).primaryColor),
                            ),
                            child: Text(
                              "Please try again later",
                              style: TextStyle(
                                fontSize: kMainTextSize,
                                color: Theme.of(context).accentColor,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      );
                    } else {
                      return Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                                  child: SubscriptionCard(
                                    price: offerings.current.monthly.product.priceString,
                                    isMonthly: true,
                                    desc: "test",
                                    isActive: monthlySelected,
                                    onTap: (){
                                      setState(() {
                                        monthlySelected = !monthlySelected;
                                        if(monthlySelected){
                                          yearlySelected = false;
                                        }
                                      });
                                    },
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                                  child: SubscriptionCard(
                                    price: offerings.current.annual.product.priceString,
                                    isMonthly:  false,
                                    desc: "test",
                                    isActive: yearlySelected,
                                    onTap: (){
                                      setState(() {
                                        yearlySelected = !yearlySelected;
                                        if(yearlySelected){
                                          monthlySelected = false;
                                        }
                                      });
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                          GestureDetector(
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: height * 0.03),
                              child: Container(
                                width: width * 0.8,
                                padding: EdgeInsets.symmetric(vertical: height * 0.015),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(color: Theme.of(context).primaryColor),
                                ),
                                child: Text(
                                  "Continue",
                                  style: TextStyle(
                                    fontSize: kMainTextSize,
                                    color: Theme.of(context).accentColor,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            onTap: () async {
                              Product product = await getProduct();
                              await Purchases.purchaseProduct(product.identifier);
                              userIsSubscriber = await isSubscriber();
                            },
                          ),
                        ],
                      );
                    }
                  } else {
                    return SizedBox();
                  }
                },
              ),

            ],
          ),
        ),
      ),
    );
  }

  Future<Product> getProduct() async{
    Offerings offerings = await Purchases.getOfferings();
    if(offerings.current != null){
      if(yearlySelected){
        return offerings.current.annual.product;
      } else {
        return offerings.current.monthly.product;
      }
    } else {
      return null;
    }
  }

}



class FeatureRow extends StatelessWidget {
  FeatureRow({
    @required this.text,
  });

  final String text;

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.symmetric(vertical: height * 0.015),
      child: Stack(
        children: [
          AutoSizeText(
            text,
            maxLines: 1,
            style: TextStyle(
                fontSize: kMainTextSize,
                color: Theme.of(context).accentColor,
              ),
          ),
          SizedBox(width: width * 0.1,),
          Align(
            alignment: Alignment.centerRight,
            child: Icon(
              FontAwesomeIcons.check,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ],
      ),
    );
  }
}
